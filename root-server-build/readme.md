**Настройка окружения**

Для каждого сервера используется свое доменное, поэтому в 'hosts' необходимо прописать:

    127.0.0.1       root.server
    127.0.0.1       root.server.ws

Установить raven-ca.crt (сертификат корневого CA) в хранилище доверенных корневых сертификатов браузера и/или операционной системы, затем
root-server-ca.crt и root-server-ws-ca.crt (необязательно) - сертификаты промежуточных CA.

Установить admin.p12 (контейнер приватного ключа и сертификата клиента root.server) в хранилище личных сертификатов браузера и/или операционной системы.

Необязательно: установить client.ws.p12 (контейнер приватного ключа и сертификата клиента root.server.ws) в хранилище личных сертификатов браузера и/или операционной системы. Необходим только для вызовов сервисов из браузера.

Необязательно: настроить postman на использование admin.p12, client.ws.p12.

Пароли ключей и хранилищ всегда одни и те же: QrzQso15

***
**Серверные ключи и сертификаты**

В рамках проекта используется для окружения разработчика корневой CA, который удостоверяет серверные и клиентские сертификаты.

Генерация приватного ключа и сертификата CA (для промежуточных CA аналогично, но с подписанием сертификата корневым CA):

    keytool -genkey -alias raven-root-ca
            -ext KU=keyCertSign -ext BC=ca:true
            -keyalg RSA -keysize 4096 -sigalg SHA512withRSA
            -keypass QrzQso15 -validity 10000
            -dname "CN=Raven Root CA,O=Raven Soft,C=RU"
            -keystore raven-root-ca.p12 -storepass QrzQso15 -deststoretype pkcs12

Экспорт сертификата в файл в кодировке DER (необходим для установки в кейсторы клиентов, серверов, операционной системы или браузеров):

    keytool -exportcert -keystore raven-root-ca.p12 -storepass QrzQso15 -alias raven-root-ca -file raven-root-ca.crt

Генерация серверного приватного ключа сервера и сертификата:

    keytool -genkey -alias root.server
            -keyalg RSA -keysize 4096 -sigalg SHA512withRSA
            -keypass QrzQso15 -validity 10000
            -dname "CN=root.server,O=Raven Soft,C=RU"
            -keystore root.server.p12 -storepass QrzQso15 -deststoretype pkcs12

Создание запроса на подпись сертификата:

    keytool -certreq -alias root.server
            -keyalg RSA -keysize 4096 -sigalg SHA512withRSA
            -validity 10000 -file root.server.csr
            -keystore root.server.p12 -storepass QrzQso15

Подпись CA:

    keytool -gencert -alias raven-root-ca
            -ext SAN=dns:root.server,ip:127.0.0.1 -ext EKU=serverAuth
            -validity 10000 -sigalg SHA512withRSA
            -infile root.server.csr -outfile root.server.crt
            -keystore raven-root-ca.p12 -storepass QrzQso15

Импорт публичного ключа CA в серверный keystore:

    keytool -import -trustcacerts -alias ca
            -file raven-root-ca.crt -keystore root.server.p12 -storepass QrzQso15 -noprompt

Импорт публичного ключа, подписанного CA, в серверный keystore:

    keytool -importcert -alias root.server -file root.server.crt
            -keystore root.server.p12 -storepass QrzQso15

Truststore нет смысла создавать, поскольку CA один и тот же и уже присутствует в keystore.

Генерация клиентского приватного ключа сервера и сертификата:

    keytool -genkey -alias admin
            -keyalg RSA -keysize 4096 -sigalg SHA512withRSA
            -keypass QrzQso15 -validity 10000
            -dname "CN=admin,O=Root Server,C=RU" -keystore admin.p12
            -storepass QrzQso15 -deststoretype pkcs12

Создание запроса на подпись сертификата:

    keytool -certreq -alias admin
            -keyalg RSA -keysize 4096 -sigalg SHA512withRSA
            -validity 10000 -file admin.csr
            -keystore admin.p12 -storepass QrzQso15

Подпись CA:

    keytool -gencert -alias raven-root-ca
            -ext EKU=clientAuth -validity 10000 -sigalg SHA512withRSA
            -infile admin.csr -outfile admin.crt
            -keystore raven-root-ca.p12 -storepass QrzQso15

Импорт публичного ключа CA в клиентский keystore:

    keytool -import -trustcacerts -alias ca
            -file raven-root-ca.crt -keystore admin.p12 -storepass QrzQso15 -noprompt

Импорт публичного ключа, подписанного CA, в клиентский keystore:

    keytool -importcert -alias admin -file admin.crt
            -keystore admin.p12 -storepass QrzQso15

Полные тесты скриптов см. в файле `crypto.scripts`.

***
**Хэширование паролей в application.yml**

    java -cp jasypt-1.9.3.jar org.jasypt.intf.cli.JasyptPBEStringEncryptionCLI
         input="somePasswordToHash" password=someSecretKey

***
**Установка time zone на MySql сервере**

Если на MySql сервере не установить временную зону, то при выполнении запросов с использованием JDBC драйвера возникнет исключение. Решение:

    SET GLOBAL time_zone = '+3:00';
    SELECT @@global.time_zone;

или прописать в my.ini:

    default-time-zone='+3:00'

***
**Важно**
* В промышленной среде должна быть настроена IP фильтрация для клиентов сервисов (т.е. фактических потребителей).
* В промышленной среде должен быть отдельный корневой CA, промежуточные CA для каждого веб сервера (для подписания сертификатов серверов и клиентов). Совпадающие пароли для keystore и private key указать только для p12, созднного для установки в ОС, т.е. только пользователи root-server (i.e. admins).
