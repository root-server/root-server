package root.server.common.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import root.server.common.entity.base.IdEntity;

/**
 * Client entity class.
 * @author Alexey Chalov
 */
@Getter
@Setter
@ToString(callSuper = true)
@SuppressWarnings("serial")
public class Client extends IdEntity {

    @NotNull(message = "{root.server.validation.client.certCN.NotNull.message}")
    @Size(max = 128, message = "{root.server.validation.client.certCN.Size.message}")
    @Pattern(
        regexp = "\\b((?=[a-z0-9-]{1,63}\\.)(xn--)?[a-z0-9]+(-[a-z0-9]+)*\\.)+[a-z]{2,63}\\b$",
        message = "{root.server.validation.client.certCN.Pattern.message}"
    )
    private String certCN;

    @Size(max = 128, message = "{root.server.validation.client.organizationName.Size.message}")
    private String organizationName;

    @Size(max = 128, message = "{root.server.validation.client.contactPersonFullName.Size.message}")
    private String contactPersonFullName;

    @Pattern(
        regexp = "^|\\+7 \\([\\d]{3}\\) [\\d]{3}-[\\d]{2}-[\\d]{2}$",
        message = "{root.server.validation.client.contactPersonPhone.Pattern.message}"
    )
    private String contactPersonPhone;

    @Email(message = "{root.server.validation.client.notificationEmail.Email.message}")
    @Size(max = 128, message = "{root.server.validation.client.notificationEmail.Size.message}")
    private String notificationEmail;

    private BigDecimal balance;
    private LocalDate paidTill;
}
