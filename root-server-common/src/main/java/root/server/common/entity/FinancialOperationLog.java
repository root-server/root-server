package root.server.common.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import root.server.common.entity.base.IdEntity;
import root.server.common.validation.CorrectId;

/**
 * Financial operation log entity class.
 * @author Alexey Chalov
 */
@Getter
@Setter
@ToString(callSuper = false)
@SuppressWarnings("serial")
public class FinancialOperationLog extends IdEntity {

    @CorrectId(
        fieldName = "operationType",
        message = "{root.server.validation.financialOperationLog.operationType.CorrectId.message}"
    )
    private FinancialOperationType operationType;

    @ToString.Include(rank = 2)
    @NotNull(message = "{root.server.validation.financialOperationLog.balanceChange.NotNull.message}")
    @DecimalMin(
        value = "0.01", message = "{root.server.validation.financialOperationLog.balanceChange.DecimalMin.message}"
    )
    @Digits(
        integer = Integer.MAX_VALUE, fraction = 2,
        message = "{root.server.validation.financialOperationLog.balanceChange.Digits.message}"
    )
    private BigDecimal balanceChange;

    @ToString.Include(rank = 1)
    @NotNull(message = "{root.server.validation.financialOperationLog.paidTill.NotNull.message}")
    private LocalDate paidTill;

    @Size(max = 256, message = "{root.server.validation.financialOperationLog.reason.Size.message}")
    private String reason;

    private User performer;
    private BigDecimal balance;
    private LocalDateTime executionDate;
}
