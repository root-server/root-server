package root.server.common.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import root.server.common.entity.base.IdNameEntity;

/**
 * Financial operation type entity class.
 * @author Alexey Chalov
 */
@Getter
@Setter
@ToString(callSuper = true)
@SuppressWarnings("serial")
public class FinancialOperationType extends IdNameEntity {

    @JsonIgnore
    private boolean charge;
    private boolean reasonRequired;
}
