package root.server.common.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import root.server.common.entity.base.IdNameEntity;

/**
 * Manufacturer entity class.
 * @author Alexey Chalov
 */
@Getter
@Setter
@ToString(callSuper = true)
@SuppressWarnings("serial")
public class Manufacturer extends IdNameEntity {

    private boolean genuine;
    private ManufacturerGroup manufacturerGroup;
}
