package root.server.common.entity;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import root.server.common.entity.base.IdNameEntity;

/**
 * Manufacturer group entity class.
 * @author Alexey Chalov
 */
@Getter
@Setter
@ToString(callSuper = true)
@SuppressWarnings("serial")
public class ManufacturerGroup extends IdNameEntity {

    private List<Manufacturer> manufacturers;
    private String manufacturerNames;
}
