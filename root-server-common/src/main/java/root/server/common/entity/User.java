package root.server.common.entity;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import root.server.common.entity.base.IdEntity;

/**
 * User entity class.
 * @author Alexey Chalov
 */
@Getter
@Setter
@ToString(callSuper = true)
@SuppressWarnings("serial")
public class User extends IdEntity {

    @NotNull(message = "{root.server.validation.user.certCN.NotNull.message}")
    @Size(min = 1, max = 128, message = "{root.server.validation.user.certCN.Size.message}")
    private String certCN;

    private Boolean active;
}
