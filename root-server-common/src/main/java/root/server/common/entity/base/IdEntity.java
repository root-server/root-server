package root.server.common.entity.base;

import java.io.Serializable;

import lombok.Data;

/**
 * Base for entities, that contains identifier field.
 * @author Alexey Chalov
 */
@Data
@SuppressWarnings("serial")
public class IdEntity implements Serializable {

    private Long id;
}
