package root.server.common.entity.base;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Base superclass for entities, that contains identifier and name fields.
 * @author Alexey Chalov
 */
@Getter
@Setter
@ToString(callSuper = true)
@SuppressWarnings("serial")
public abstract class IdNameEntity extends IdEntity {

    private String name;
}
