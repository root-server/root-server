package root.server.common.entity.base;

import java.util.List;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Represents object list paginated data.
 * @author Alexey Chalov
 * @param <T> object list type
 */
@Getter
@RequiredArgsConstructor
public class PageData<T> {

    private final int total;
    private final List<T> data;
}
