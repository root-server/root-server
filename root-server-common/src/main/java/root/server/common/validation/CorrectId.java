package root.server.common.validation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * Marker annotation for {@link root.server.common.entity.base.IdEntity} entity identifier is not null
 * and corresponding record exists in database validation.
 * @author Alexey Chalov
 */
@Target(FIELD)
@Retention(RUNTIME)
@Constraint(validatedBy = {})
public @interface CorrectId {

    /**
     * Validation failure message.
     * @return validation failure message
     */
    String message() default "{root.server.validation.entity.CorrectId.message}";

    /**
     * Group participants.
     * @return {@link Class} array
     */
    Class<?>[] groups() default {};

    /**
     * Payload array.
     * @return {@link Payload} array
     */
    Class<? extends Payload>[] payload() default {};

    /**
     * Returns field name for validation.
     * @return field name for validation
     */
    String fieldName();
}
