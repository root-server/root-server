package root.server.common.validation;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Validation error container.
 * @author Alexey Chalov
 */
@Getter
@RequiredArgsConstructor
public class ValidationError {

    private final String objectName;
    private final String fieldName;
    private final String errorCode;
    private final String defaultMessage;
}
