package root.server.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Transactional;

import root.server.common.entity.FinancialOperationType;

/**
 * Classifier operations DAO.
 * @author Alexey Chalov
 */
@Mapper
public interface ClassifierDao {

    /**
     * Returns list of financial operation types visible on UI.
     * @return list of financial operation types visible on UI
     */
    @Transactional(readOnly = true)
    List<FinancialOperationType> getVisibleFinancialOperationTypes();

    /**
     * Returns {@link FinancialOperationType} instance found by identifier that is visible on UI.
     * @param id financial operation type identifier
     * @return {@link FinancialOperationType} instance
     */
    @Transactional(readOnly = true)
    FinancialOperationType getVisibleFinancialOperationType(@Param("id") Long id);

    /**
     * Returns true, if financial operation type with passed identifier exists, false otherwise.
     * @param id financial operation type identifier
     * @return boolean
     */
    @Transactional(readOnly = true)
    boolean isVisibleFinancialOperationTypeExists(@Param("id") Long id);
}
