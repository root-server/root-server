package root.server.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Transactional;

import root.server.common.entity.Client;
import root.server.common.entity.FinancialOperationLog;

/**
 * Client operations DAO.
 * @author Alexey Chalov
 */
@Mapper
public interface ClientDao {

    /**
     * Returns client found by identifier.
     * @param id client identifier
     * @return {@link Client} instance
     */
    @Transactional(readOnly = true)
    Client getById(@Param("id") Long id);

    /**
     * Returns client found by X509 certificate CN.
     * @param certCN X509 certificate CN
     * @return {@link Client} instance
     */
    @Transactional(readOnly = true)
    Client getByCN(@Param("certCN") String certCN);

    /**
     * Returns full list of clients.
     * @return full list of clients
     */
    @Transactional(readOnly = true)
    List<Client> getClients();

    /**
     * Returns true, if client with passed identifier exists in database, false otherwise.
     * @param id client identifier
     * @return boolean
     */
    @Transactional(readOnly = true)
    boolean isClientWithIdExists(@Param("id") Long id);

    /**
     * Returns true, if client with passed <code>certCN</code> already exists, false otherwise.
     * @param clientId client identifier
     * @param certCN client certificate CN RDN
     * @return boolean
     */
    @Transactional(readOnly = true)
    boolean isClientWithCertCnExists(@Param("clientId") Long clientId, @Param("certCN") String certCN);

    /**
     * Creates new client.
     * @param client {@link Client} instance
     */
    @Transactional
    void createClient(@Param("client") Client client);

    /**
     * Updates existing client.
     * @param client {@link Client} instance
     */
    @Transactional
    void updateClient(@Param("client") Client client);

    /**
     * Changes client balance, writes financial operation log.
     * @param clientId client identifier
     * @param financialOperationLog {@link FinancialOperationLog} instance
     */
    @Transactional
    void changeClientBalance(@Param("clientId") Long clientId,
                             @Param("financialOperationLog") FinancialOperationLog financialOperationLog);

    /**
     * Returns list of client financial operations.
     * @param clientId client identifier
     * @param offset first record offset
     * @param limit limit of records to retrieve
     * @return list of client financial operations
     */
    @Transactional(readOnly = true)
    List<FinancialOperationLog> getFinancialOperations(@Param("clientId") Long clientId,
                                                       @Param("offset") int offset, @Param("limit") int limit);

    /**
     * Returns total number of client financial operations.
     * @param clientId client identifier
     * @return total number of client financial operations
     */
    @Transactional(readOnly = true)
    int getFinancialOperationCount(@Param("clientId") Long clientId);
}
