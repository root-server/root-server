package root.server.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.transaction.annotation.Transactional;

import root.server.common.entity.ManufacturerGroup;

/**
 * Manufacturer operations DAO.
 * @author Alexey Chalov
 */
@Mapper
public interface ManufacturerDao {

    /**
     * Returns list of manufacturer groups.
     * @return list of manufacturer groups
     */
    @Transactional(readOnly = true)
    List<ManufacturerGroup> getGroups();
}
