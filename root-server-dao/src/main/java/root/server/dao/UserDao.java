package root.server.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Transactional;

import root.server.common.entity.User;

/**
 * User operations DAO.
 * @author Alexey Chalov
 */
@Mapper
public interface UserDao {

    /**
     * Returns user found by identifier.
     * @param id user identifier
     * @return {@link User} instance
     */
    @Transactional(readOnly = true)
    User getById(@Param("id") Long id);

    /**
     * Returns active user found by X509 certificate CN RDN.
     * @param certCN X509 certificate CN
     * @return {@link User} instance
     */
    @Transactional(readOnly = true)
    User getByCN(@Param("certCN") String certCN);

    /**
     * Returns full list of users.
     * @return full list of users
     */
    @Transactional(readOnly = true)
    List<User> getUsers();

    /**
     * Returns true, if user with passed identifier exists in database, false otherwise.
     * @param id user identifier
     * @return boolean
     */
    @Transactional(readOnly = true)
    boolean isUserWithIdExists(@Param("id") Long id);

    /**
     * Returns true, if user with passed <code>certCN</code> already exists, false otherwise.
     * @param userId user identifier
     * @param certCN user certificate CN RDN
     * @return boolean
     */
    @Transactional(readOnly = true)
    boolean isUserWithCertCnExists(@Param("userId") Long userId, @Param("certCN") String certCN);

    /**
     * Creates new user.
     * @param user {@link User} instance
     */
    @Transactional
    void createUser(@Param("user") User user);

    /**
     * Updates existing user.
     * @param user {@link User} instance
     */
    @Transactional
    void updateUser(@Param("user") User user);
}
