package root.server.dao.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Root server DAO configuration.
 * @author Alexey Chalov
 */
@Configuration
@EnableTransactionManagement
@MapperScan("root.server.dao")
public class RootServerDaoConfiguration {

}
