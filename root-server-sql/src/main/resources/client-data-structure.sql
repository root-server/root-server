--liquibase formatted sql

--changeset alexey:client-data-structure_1
create table client(
    id        mediumint unsigned auto_increment not null,
    cert_cn   varchar(128)                      not null,
    balance   decimal(10, 4)                    not null default 0,
    paid_till timestamp                             null,
    primary key(id),
    constraint client_uk01 unique(cert_cn)
) engine = InnoDb;

create table user(
    id        mediumint unsigned auto_increment not null,
    cert_cn   varchar(128)                      not null,
    active    tinyint(1) unsigned               not null default 1,
    primary key(id),
    constraint user_uk01 unique(cert_cn)
) engine = InnoDb;

--changeset alexey:client-data-structure_2
alter table client change column paid_till paid_till date null;

alter table client add column organization_name        varchar(128) after cert_cn;
alter table client add column contact_person_full_name varchar(128) after organization_name;
alter table client add column contact_person_phone     char(18)     after contact_person_full_name;
alter table client add column notification_email       varchar(64)  after contact_person_phone;

--changeset alexey:client-data-structure_3
create table financial_operation_type(
    id              tinyint unsigned    not null,
    name            varchar(128)        not null,
    charge          tinyint(1) unsigned not null,
    reason_required tinyint(1) unsigned not null,
    ui_visible      tinyint(1) unsigned not null,
    primary key(id)
) engine = InnoDb;

insert into financial_operation_type(id, name, charge, reason_required, ui_visible)
values(1, 'Пополнение баланса', 0, 0, 1), (2, 'Корректировка баланса (начисление)', 0, 1, 1),
      (3, 'Корректировка баланса (списание)', 1, 1, 1), (4, 'Оплата аренды ПО', 1, 0, 0);

create table financial_operation_log(
    id                          bigint unsigned auto_increment not null,
    client_id                   mediumint unsigned             not null,
    performer_id                mediumint unsigned,
    financial_operation_type_id tinyint unsigned               not null,
    balance_change              decimal(10, 4)                 not null,
    balance                     decimal(10, 4)                 not null,
    paid_till                   date                           not null,
    execution_date              timestamp                      not null default current_timestamp,
    reason                      varchar(256),
    primary key(id),
    constraint financial_operation_log_fk01 foreign key(client_id)                   references client(id),
    constraint financial_operation_log_fk02 foreign key(performer_id)                references user(id),
    constraint financial_operation_log_fk03 foreign key(financial_operation_type_id) references financial_operation_type(id)
) engine = InnoDb;
