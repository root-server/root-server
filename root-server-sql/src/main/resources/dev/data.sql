--liquibase formatted sql

--changeset alexey:dev-data_1 contexts:dev
insert into client(cert_cn, paid_till) values('ok.parts', date_add(now(), interval 1 month));
insert into user(cert_cn) values('admin');
