create table part_number_lookup_old(
    id bigint unsigned,
    part_number_id bigint unsigned,
    number varchar(20),
    manufacturer_id smallint unsigned,
    supplier_id mediumint unsigned,
    import_date timestamp
) engine = InnoDb;

load data local infile 'part_number_lookup.dat' into table part_number_lookup_old fields terminated by '\t' lines terminated by '\r\n';

delete from part_number_lookup_old where manufacturer_id = 20;

create unique index part_number_lookup_old_idx01 on part_number_lookup_old(part_number_id, number, manufacturer_id);
create index part_number_lookup_old_idx02 on part_number_lookup_old(number, manufacturer_id);

create table part_number_lookup_old_normalized
select o.part_number_id, pn.id
  from part_number_lookup_old o join part_number pn on o.number = pn.number
                                                   and o.manufacturer_id = pn.manufacturer_id
 where o.part_number_id != pn.id;

create unique index part_number_lookup_old_normalized_idx01 on part_number_lookup_old_normalized(part_number_id, id);
alter table part_number_lookup_old_normalized add processed tinyint(1) unsigned not null default 0;

delete from part_number_lookup_old_normalized
 where part_number_id in (
    select part_number_id from part_number_lookup_old_normalized o left join part_number n on o.part_number_id = n.id
     where n.id is null
);

create index part_number_lookup_old_normalized_idx02 on part_number_lookup_old_normalized(processed);
create index part_number_lookup_old_normalized_idx03 on part_number_lookup_old_normalized(part_number_id);
create index part_number_lookup_old_normalized_idx04 on part_number_lookup_old_normalized(id);

drop procedure if exists  migrate_okparts_crosses;

create procedure migrate_okparts_crosses()
begin
    declare _current_pn_id       bigint unsigned;
    declare _inner_current_pn_id bigint unsigned;
    declare _cross_group_id      bigint unsigned;

    create temporary table if not exists _cross_part_number_group(
        part_number_id bigint     unsigned,
        processed      tinyint(1) unsigned not null default 0,
        primary key(part_number_id)
    ) engine = Memory;

    pnloop: while true do
        start transaction;
        truncate table _cross_part_number_group;

        select part_number_id into _current_pn_id
          from part_number_lookup_old_normalized
         where processed = 0
         limit 1;
        if _current_pn_id is null then
            leave pnloop;
        end if;

        set _inner_current_pn_id := _current_pn_id;

        innerpnloop: while true do
            if _inner_current_pn_id is null then
                leave innerpnloop;
            end if;

            insert into _cross_part_number_group(part_number_id, processed) values(_inner_current_pn_id, 1)
                on duplicate key update processed = 1;

            insert ignore into _cross_part_number_group(part_number_id)
            select part_number_id
              from part_number_lookup_old_normalized
             where id = _inner_current_pn_id;
            insert ignore into _cross_part_number_group(part_number_id)
            select id
              from part_number_lookup_old_normalized
             where part_number_id = _inner_current_pn_id;

            set _inner_current_pn_id := null;
            select part_number_id into _inner_current_pn_id
              from _cross_part_number_group
             where processed = 0
             limit 1;
        end while innerpnloop;

        select part_number_ss_cross_seq_nextval() into _cross_group_id;
        insert into part_number_ss_cross(part_number_id, ss_cross_group_id)
        select part_number_id, _cross_group_id
          from _cross_part_number_group;

        update part_number_lookup_old_normalized
           set processed = 1
         where part_number_id in (
             select part_number_id
               from _cross_part_number_group
         );
        update part_number_lookup_old_normalized
           set processed = 1
         where id in (
             select part_number_id
               from _cross_part_number_group
         );

        set _current_pn_id := null;
        commit;
    end while pnloop;
    commit;
end;

call migrate_okparts_crosses();

drop procedure migrate_okparts_crosses;
drop table part_number_lookup_old_normalized;
drop table part_number_lookup_old;

/* общее количество кроссов */
select sum(cnt * (cnt - 1) / 2)
 from (
    select ss_cross_group_id, count(part_number_id) cnt
      from part_number_ss_cross
     group by ss_cross_group_id
 ) t;

/* поиск кроссов */
select id, display_number, manufacturer_id
  from part_number
 where number = '34161SA000' and manufacturer_id = 137
 union
 select pn.id, pn.display_number, pn.manufacturer_id
   from part_number pn join part_number_ss_cross c on pn.id = c.part_number_id
                       join part_number_ss_cross c0 on c0.ss_cross_group_id = c.ss_cross_group_id
                       join part_number pn0 on pn0.id = c0.part_number_id and pn0.number = '34161SA000'
                                                                          and pn0.manufacturer_id = 137
/* поиск кроссов - 2 */
with t as (
    select pn0.id, pn0.display_number, pn0.manufacturer_id, c0.ss_cross_group_id
      from part_number pn0 left join part_number_ss_cross c0 on pn0.id = c0.part_number_id
     where pn0.number = '34161SA000' and pn0.manufacturer_id = 137
)
select t.id, t.display_number, t.manufacturer_id
  from t
 union
 select pn.id, pn.display_number, pn.manufacturer_id
  from part_number pn join part_number_ss_cross c on pn.id = c.part_number_id join t on c.ss_cross_group_id = t.ss_cross_group_id
