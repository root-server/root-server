/* удаление пустых записей или без соотвествующего артикула */
delete from part_name_old where part_number_id in (
    select pno.part_number_id
      from part_name_old pno left join part_number pn on pno.part_number_id = pn.id
     where pn.id is null
        or (
            (pno.name is null or trim(pno.name) = '')
            and
            (pno.name_ru is null or trim(pno.name_ru) = '')
        )
);

/* поиск русского нименования в колонке, где должно быть английское наименование */
select *
  from part_name_old
 where name regexp '[а-яА-Я]'
   and (name_ru is null or trim(name_ru) = '')
 order by name;

 /* перенос данных между колонками (русское наименование в английской колонке в колонку с русским наименованием) */
 update part_name_old
   set name_ru = name, name = null
 where name regexp '[а-яА-Я]'
   and (name_ru is null or trim(name_ru) = '');

/* поиск английского наименования в колонке с русским наименованием */
select *
  from part_name_old
 where name_ru regexp '^[a-zA-Z]$'
   and (name is null or trim(name) = '')
 order by name_ru;

/* избавляемся от пустых строк */
update part_name_old
   set name = null
 where trim(name) = '';
update part_name_old
   set name_ru = null
 where trim(name_ru) = '';

/* поиск русского наименования в колонке с английским наименованием при совпадающих значениях */
select name, part_number_id
 from part_name_old
 where name = name_ru
   and name regexp '[а-яА-Я]'
 order by name;

/* удаление русского наименования в колонке с английским наименованием при совпадающих значениях */
update part_name_old
   set name = null
 where name = name_ru
   and name regexp '[а-яА-Я]';

/* поиск английского наименования в колонке с русским наименованием при совпадающих значениях */
select name_ru, part_number_id
 from part_name_old
 where name = name_ru
   and name_ru not regexp '[а-яА-Я]'
 order by name_ru;

/* удаление английского наименования в колонке с русским наименованием при совпадающих значениях */
update part_name_old
   set name_ru = null
 where name = name_ru
   and name_ru not regexp '[а-яА-Я]';

/* оставшиеся ошибочные наименования */
select *
 from part_name_old
 where name regexp '[а-яА-Я]'
    or name_ru not regexp '[а-яА-Я]';
