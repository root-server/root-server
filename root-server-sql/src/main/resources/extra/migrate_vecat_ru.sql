/* есть ссылки на алиас производителя */
truncate table search_log;

/* поиск ссылок в номерах на алиас производителя */
select pn.manufacturer_id, m.name, m.parent_id, mp.name parent_name, pn.number
 from part_number pn join manufacturer m on pn.manufacturer_id = m.id and m.parent_id is not null
                     join manufacturer mp on m.parent_id = mp.id
order by pn.manufacturer_id;

/* удяляем записи с алиасом производителей */
delete part_number
  from part_number join manufacturer on part_number.manufacturer_id = manufacturer.id and manufacturer.parent_id is not null;
delete part_number_lookup
 from part_number_lookup join manufacturer on part_number_lookup.manufacturer_id = manufacturer.id and manufacturer.parent_id is not null;

/* удаляем алиасы производителей */
 delete from manufacturer where parent_id is not null;

/* удаляем номера производителей китайских фабрик, наименования и лукап удалятся по каскаду */
delete from part_number where manufacturer_id = 20;

/* удаляем производителей китайских фабрик */
delete from manufacturer where id = 20;

/* нормализуем наименования и удаляем пустые */
update part_name set name = replace(name, '\r\n', ' ');
update part_name set name = replace(name, '\r', ' ');
update part_name set name = replace(name, '\n', ' ');
update part_name set name = replace(name, '\t', ' ');
update part_name set name = replace(name, '  ', ' ');
update part_name set name = trim(name);
delete from part_name
 where name is null or length(name) < 2;

/* создаем таблицу нормализованных кроссов */
create table part_number_lookup_normalized(
    part_number_id  bigint unsigned,
    part_number_id2 bigint unsigned
) engine = InnoDb;

/* заполняем таблицу нормализованных кроссов */
insert into part_number_lookup_normalized(part_number_id, part_number_id2)
select pnl.part_number_id, pn.id part_number_id2
  from part_number_lookup pnl use index (primary)
                              join part_number pn on pn.number = pnl.number and pnl.manufacturer_id = pn.manufacturer_id
order by pnl.id;

/* создаем таблицу с уникальными кроссировками (номер сам на себя - не уникальная кроссировка, не нужна) */
create table part_number_lookup_normalized2(
    part_number_id  bigint unsigned,
    part_number_id2 bigint unsigned
) engine = InnoDb;

/* заполняем таблицу уникальных кроссировок */
insert into part_number_lookup_normalized2(part_number_id, part_number_id2)
select part_number_id, part_number_id2
  from part_number_lookup_normalized
 where part_number_id != part_number_id2;

/* удаляем нерелевантные символы внутри артикулов */
update part_number set display_number = replace(display_number, ' ', '');
update part_number set display_number = replace(display_number, '*', '');
delete from part_number where trim(display_number) = '';
delete from part_number where trim(number) = '';
delete from part_number where number = 'na';

/* удалем номера мерседеса начинающиеся с цифр */
delete from part_number where manufacturer_id = 40 and number regexp '^[0-9]{1}[a-z0-9]*$';

/* проверочный запрос для анализа оставшихся номеров */
select *
  from part_number pn use index (primary) join manufacturer m on pn.manufacturer_id = m.id
 where not pn.display_number regexp '^[a-z0-9\-\.]*$';

/* очистка таблицы кроссов */
delete part_number_lookup_normalized2
  from part_number_lookup_normalized2 left join part_number on part_number_lookup_normalized2.part_number_id = part_number.id
 where part_number.id is null;
delete part_number_lookup_normalized2
  from part_number_lookup_normalized2 left join part_number on part_number_lookup_normalized2.part_number_id2 = part_number.id
 where part_number.id is null;

/* миграция недостающих производителей */
insert into root_server.manufacturer(name, is_genuine)
select m.name, m.is_genuine
  from manufacturer m left join root_server.manufacturer rm on m.name = rm.name
 where rm.name is null;

/* миграция номеров и наименований */
create procedure migrate_part_numbers(source_id smallint unsigned)
begin
    declare record_count         smallint unsigned default 0;

    declare curr_pn_id           bigint unsigned   default 0;

    declare pn_id                bigint unsigned;
    declare pn_number            varchar(20);
    declare pn_display_number    varchar(32);
    declare pn_name              varchar(512);
    declare pn_name_lang         char(2);
    declare mf_id                smallint unsigned;

    declare ex_pn_id             bigint unsigned;
    declare ex_pn_display_number varchar(32);
    declare ex_pn_name           varchar(512);

    start transaction;
    pnloop: while true do
        set pn_id := null,
            pn_number := null,
            pn_display_number := null,
            pn_name := null,
            pn_name_lang := null,
            mf_id := null,
            ex_pn_id = null,
            ex_pn_display_number = null,
            ex_pn_name := null;

        select pn.id, pn.number, pn.display_number, rm.id manufacturer_id, trim(pname.name),
               case when pname.name regexp '[а-яА-Я]' then 'RU' else 'EN' end
          into pn_id, pn_number, pn_display_number, mf_id, pn_name, pn_name_lang
          from part_number pn use index (primary)
                              join manufacturer m on pn.manufacturer_id = m.id
                              join root_server.manufacturer rm on m.name = rm.name
                              left join part_name pname on pname.part_number_id = pn.id
        where pn.id > curr_pn_id
        order by pn.id
        limit 1;

        if pn_id is null then
            leave pnloop;
        end if;

        select id, display_number
          into ex_pn_id, ex_pn_display_number
          from root_server.part_number
         where number = pn_number
           and manufacturer_id = mf_id;

        if ex_pn_id is not null then
            if length(pn_display_number) > length(ex_pn_display_number) then
                update root_server.part_number
                   set display_number = pn_display_number
                 where id = ex_pn_id;
            end if;

            if pn_name is not null then
                select name
                  into ex_pn_name
                  from root_server.part_name
                 where part_number_id = ex_pn_id
                   and language = pn_name_lang;

                if ex_pn_name is not null then
                    if length(pn_name) > length(ex_pn_name) then
                        update root_server.part_name
                           set name = pn_name
                         where part_number_id = ex_pn_id
                           and language = pn_name_lang;
                    end if;
                else
                    insert into root_server.part_name(part_number_id, name, language)
                    values(ex_pn_id, pn_name, pn_name_lang);
                end if;
            end if;
        else
            insert into root_server.part_number(number, manufacturer_id, display_number)
            values(pn_number, mf_id, pn_display_number);
            set ex_pn_id := last_insert_id();

            if pn_name is not null then
                insert into root_server.part_name(part_number_id, name, language)
                values(ex_pn_id, pn_name, pn_name_lang);
            end if;
        end if;

        insert into root_server.part_number_source(part_number_id, origin_source_id)
        values(ex_pn_id, source_id);

        set curr_pn_id := pn_id;

        set record_count := record_count + 1;
        if record_count = 10000 then
            set record_count := 0;
            commit;
            start transaction;
        end if;
     end while pnloop;
     commit;
end;

call migrate_part_numbers(2);

/* проверочный запрос, что переливка номеров прошла успешно, должен выдать 0 */
select count(1)
  from part_number ppn use index (primary)
                                      join manufacturer pm on ppn.manufacturer_id = pm.id
                                 left join root_server.part_number pn on ppn.number = pn.number
                                 left join root_server.manufacturer m on pn.manufacturer_id = m.id and pm.name = m.name
 where pn.id is null;

/* создаем таблицу связку перелитых номеров */
create table part_number_lookup_link(
    pt_part_number_id  bigint unsigned,
    rs_part_number_id bigint unsigned
) engine = InnoDb;

/* заполняем таблицу связку перелитых номеров */
insert into part_number_lookup_link(pt_part_number_id, rs_part_number_id)
select ppn.id, pn.id
  from part_number ppn use index (primary)
                                      join manufacturer pm on ppn.manufacturer_id = pm.id
                                      join root_server.part_number pn on ppn.number = pn.number
                                      join root_server.manufacturer m on pn.manufacturer_id = m.id and pm.name = m.name;

/* создаем индекс для переливки в результирующую таблицу кроссов */
create index part_number_lookup_link_idx01 on part_number_lookup_link(pt_part_number_id);

/* финальная таблица кроссировок для схемы root_server */
create table part_number_lookup_normalized_final(
    part_number_id  bigint unsigned,
    part_number_id2 bigint unsigned
) engine = InnoDb;

/* заполнение финальной таблицы кроссировок для схемы root_server */
insert into part_number_lookup_normalized_final(part_number_id, part_number_id2)
select ll0.rs_part_number_id, ll1.rs_part_number_id
  from part_number_lookup_normalized2 l join part_number_lookup_link ll0 on l.part_number_id = ll0.pt_part_number_id
                                        join part_number_lookup_link ll1 on l.part_number_id2 = ll1.pt_part_number_id;

/* кроссировки, конфликтующие с загруженными */
select count(1) --1408
  from part_number_lookup_normalized_final lf join root_server.part_number_ss_cross c1 on lf.part_number_id = c1.part_number_id
                                              join root_server.part_number_ss_cross c2 on lf.part_number_id2 = c2.part_number_id
 where c1.ss_cross_group_id != c2.ss_cross_group_id;

/* удаляем конфликтующие кроссировки */
delete part_number_lookup_normalized_final
  from part_number_lookup_normalized_final join root_server.part_number_ss_cross c1 on part_number_lookup_normalized_final.part_number_id = c1.part_number_id
                                           join root_server.part_number_ss_cross c2 on part_number_lookup_normalized_final.part_number_id2 = c2.part_number_id
 where c1.ss_cross_group_id != c2.ss_cross_group_id;

/* добавляем флаг обработки и индексы */
alter table part_number_lookup_normalized_final add column processed tinyint(1) unsigned not null default 0;
create index part_number_lookup_normalized_final_idx01 on part_number_lookup_normalized_final(processed);
create index part_number_lookup_normalized_final_idx02 on part_number_lookup_normalized_final(part_number_id);
create index part_number_lookup_normalized_final_idx03 on part_number_lookup_normalized_final(part_number_id2);

/* таблица несовместимых кроссов (уже залиты и имеют разные группы) */
create table part_number_cross_incompatible(
    part_number_id bigint unsigned not null
) engine = InnoDb;

/* миграция кроссов */
create procedure migrate_part_number_crosses(source_id smallint unsigned)
begin
    declare _current_pn_id  bigint unsigned;
    declare _cross_group_id bigint unsigned;
    declare _group_count    int    unsigned;

    create temporary table if not exists _cross_part_number_group(
        part_number_id bigint     unsigned,
        primary key(part_number_id)
    ) engine = Memory;

    pnloop: while true do
        start transaction;
        truncate table _cross_part_number_group;

        select part_number_id2 into _current_pn_id
          from part_number_lookup_normalized_final
         where processed = 0
         limit 1;

        if _current_pn_id is null then
            leave pnloop;
        end if;

        insert ignore into _cross_part_number_group(part_number_id)
        values(_current_pn_id);

        insert ignore into _cross_part_number_group(part_number_id)
        select part_number_id
          from part_number_lookup_normalized_final
         where part_number_id2 = _current_pn_id;

        select count(distinct c.ss_cross_group_id) into _group_count
          from _cross_part_number_group g join root_server.part_number_ss_cross c on g.part_number_id = c.part_number_id;

        if _group_count > 1 then
            /* several groups count => write warning log and continue */
            insert into part_number_cross_incompatible(part_number_id)
            values(_current_pn_id);
        else
            /* determine cross group identifier */
            select min(c.ss_cross_group_id) into _cross_group_id
              from _cross_part_number_group g join root_server.part_number_ss_cross c on g.part_number_id = c.part_number_id;
            if _cross_group_id is null then
                select root_server.part_number_ss_cross_seq_nextval() into _cross_group_id;
            end if;

            /* insert part number crosses */
            insert into root_server.part_number_ss_cross(part_number_id, ss_cross_group_id)
            select g.part_number_id, _cross_group_id
              from _cross_part_number_group g left join root_server.part_number_ss_cross c on g.part_number_id = c.part_number_id
             where c.id is null;

            /* insert part number cross sources */
            insert ignore into root_server.part_number_ss_cross_source(part_number_ss_cross_id, origin_source_id)
            select c.id, source_id
              from _cross_part_number_group g join root_server.part_number_ss_cross c on g.part_number_id = c.part_number_id;
        end if;

        /* update processed flag on part_number_lookup_normalized_final table */
        update part_number_lookup_normalized_final
           set processed = 1
         where part_number_id2 = _current_pn_id;

        set _cross_group_id := null, _current_pn_id := null;
        commit;
        start transaction;
    end while pnloop;
    commit;
end;

call migrate_part_number_crosses(2);
