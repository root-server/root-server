--liquibase formatted sql

--changeset alexey:managed-data-sp_1 endDelimiter:go
create function part_number_ss_cross_seq_nextval()
returns bigint unsigned
begin
    insert into part_number_ss_cross_seq values();
    return last_insert_id();
end
go
