--liquibase formatted sql

--changeset alexey:managed-data-structure_1
create table manufacturer_group(
    id      smallint unsigned auto_increment not null,
    name    varchar(255)                     not null,
    primary key(id),
    constraint manufacturer_group_uk01 unique(name)
) engine = InnoDb;

create table manufacturer(
    id                  smallint   unsigned auto_increment not null,
    name                varchar(255)                       not null,
    group_id            smallint   unsigned,
    is_genuine          tinyint(1) unsigned                not null default 0,
    primary key(id),
    constraint manufacturer_uk01 unique (name),
    constraint manufacturer_fk01 foreign key(group_id) references manufacturer_group(id)
) engine = InnoDb;

create table part_number(
    id                  bigint unsigned auto_increment not null,
    number              varchar(20)                    not null,
    manufacturer_id     smallint unsigned              not null,
    display_number      varchar(32)                    not null,
    primary key(id),
    constraint part_number_uk01 unique(number, manufacturer_id),
    constraint part_number_fk01 foreign key(manufacturer_id) references manufacturer(id)
) engine = InnoDB;

create table part_name(
    part_number_id bigint unsigned not null,
    name           varchar(512)    not null,
    language       char(2)         not null,
    constraint part_name_uk01 unique(part_number_id, language),
    constraint part_name_fk01 foreign key(part_number_id) references part_number(id) on delete cascade
) engine = InnoDb;

create table part_number_ss_cross_seq(
    id bigint unsigned not null auto_increment,
    primary key(id)
) engine = InnoDb;

create table part_number_ss_cross(
    id                bigint unsigned not null auto_increment,
    part_number_id    bigint unsigned not null,
    ss_cross_group_id bigint unsigned not null,
    primary key(id),
    constraint part_number_ss_cross_fk01 foreign key(part_number_id)    references part_number(id) on delete cascade,
    constraint part_number_ss_cross_fk02 foreign key(ss_cross_group_id) references part_number_ss_cross_seq(id),
    constraint part_number_ss_cross_uk01 unique(part_number_id, ss_cross_group_id)
) engine = InnoDb;

--changeset alexey:managed-data-structure_2
create table origin_source(
    id     smallint unsigned not null auto_increment,
    label  varchar(128)      not null,
    primary key(id),
    constraint origin_source_uk01 unique(label)
) engine = InnoDb;

create table part_number_source(
    part_number_id   bigint   unsigned not null,
    origin_source_id smallint unsigned not null,
    primary key(part_number_id, origin_source_id),
    constraint part_number_source_fk01 foreign key(part_number_id)   references part_number(id) on delete cascade,
    constraint part_number_source_fk02 foreign key(origin_source_id) references origin_source(id)
) engine = InnoDb;

create table part_number_ss_cross_source(
    part_number_ss_cross_id bigint unsigned   not null,
    origin_source_id        smallint unsigned not null,
    primary key(part_number_ss_cross_id, origin_source_id),
    constraint part_number_ss_cross_source_fk01 foreign key(part_number_ss_cross_id) references part_number_ss_cross(id) on delete cascade,
    constraint part_number_ss_cross_source_fk02 foreign key(origin_source_id)        references origin_source(id)
) engine = InnoDb;

--changeset alexey:managed-data-structure_3
alter table part_number_ss_cross_source add column part_number_id bigint unsigned after part_number_ss_cross_id;
update part_number_ss_cross_source cs join part_number_ss_cross c on cs.part_number_ss_cross_id = c.id
   set cs.part_number_id = c.part_number_id;
alter table part_number_ss_cross_source drop foreign key part_number_ss_cross_source_fk01;
alter table part_number_ss_cross_source drop primary key;
alter table part_number_ss_cross_source drop column part_number_ss_cross_id;
alter table part_number_ss_cross_source change column part_number_id part_number_id bigint unsigned not null;
alter table part_number_ss_cross_source add constraint part_number_ss_cross_source_fk01 foreign key(part_number_id) references part_number(id) on delete cascade;
alter table part_number_ss_cross_source add primary key(part_number_id, origin_source_id);

alter table part_number_ss_cross drop column id cascade;
alter table part_number_ss_cross add primary key(part_number_id);
alter table part_number_ss_cross drop constraint part_number_ss_cross_uk01;
