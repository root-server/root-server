drop database if exists root_server;
create database if not exists root_server default character set utf8mb4 collate utf8mb4_unicode_ci;

create user 'root_server'@'localhost' identified by 'root_server';
grant all privileges on root_server.* to 'root_server'@'localhost';
