import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './components/index/index.component';
import { ClientsComponent } from './components/clients/clients.component';
import { ManufacturersComponent } from './components/manufacturers/manufacturers.component';
import { ManufacturerGroupsComponent } from './components/manufacturer-groups/manufacturer-groups.component';
import { PartNumbersComponent } from './components/part-numbers/part-numbers.component';
import { AdminsComponent } from './components/admins/admins.component';
import { NotFoundComponent } from './components/errors/not-found/not-found.component';
import { InternalErrorComponent } from './components/errors/internal-error/internal-error.component';
import { NetworkErrorComponent } from './components/errors/network-error/network-error.component';
import { ForbiddenComponent } from './components/errors/forbidden/forbidden.component';

const routes: Routes = [
    { path: '', component: IndexComponent },
    { path: 'admins', component: AdminsComponent },
    { path: 'clients', component: ClientsComponent },
    { path: 'manufacturers', component: ManufacturersComponent },
    { path: 'manufacturer-groups', component: ManufacturerGroupsComponent },
    { path: 'part-numbers', component: PartNumbersComponent },
    { path: 'internal-error', component: InternalErrorComponent },
    { path: 'network-error', component: NetworkErrorComponent },
    { path: 'forbidden', component: ForbiddenComponent },
    { path: '**', component: NotFoundComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
