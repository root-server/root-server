import { Component, OnInit } from '@angular/core';
import { UserService } from './services/user.service';
import { User } from './classes/dto/user';
import { map } from 'rxjs/internal/operators/map';

/**
 * Application main component.
 */
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit{

    public currentUser: User;

    /**
     * Constructor.
     * @param userService UserService instance
     */
    constructor(private userService: UserService) {
    }

    /**
     * Initializes component.
     */
    ngOnInit(): void {
        this.userService.getCurrentUser()
            .pipe(map((user: User) => this.currentUser = user))
            .subscribe();
    }
}
