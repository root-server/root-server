import { NgModule, ɵLocaleDataIndex } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { registerLocaleData, NumberSymbol } from '@angular/common';
import localeRu from '@angular/common/locales/ru';

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { ruLocale } from 'ngx-bootstrap/locale';

import { StorageServiceModule } from 'ngx-webstorage-service';

import { NgxCleaveDirectiveModule } from 'ngx-cleave-directive';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxPrettyCheckboxModule } from 'ngx-pretty-checkbox';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ClientsComponent } from './components/clients/clients.component';
import { ManufacturersComponent } from './components/manufacturers/manufacturers.component';
import { ManufacturerGroupsComponent } from './components/manufacturer-groups/manufacturer-groups.component';
import { PartNumbersComponent } from './components/part-numbers/part-numbers.component';
import { IndexComponent } from './components/index/index.component';
import { AdminsComponent } from './components/admins/admins.component';
import { NotFoundComponent } from './components/errors/not-found/not-found.component';
import { InternalErrorComponent } from './components/errors/internal-error/internal-error.component';
import { NetworkErrorComponent } from './components/errors/network-error/network-error.component';
import { ForbiddenComponent } from './components/errors/forbidden/forbidden.component';
import { EmptyGridMessageComponent } from './components/common/empty-grid-message/empty-grid-message.component';
import { FormErrorsComponent } from './components/common/form-errors/form-errors.component';

import { HttpErrorInterceptor } from './interceptors/http-error-interceptor';
import { JsonDateInterceptor } from './interceptors/json-date-interceptor';

import { environment } from '../environments/environment';

import { CertCNValidatorDirective } from './validators/cert-cn-validator.directive';
import { MinValidatorDirective } from './validators/min-validator.directive';
import { ConditionalRequiredValidatorDirective } from './validators/conditional-required-validator.directive';

import * as LOCALIZATION from './messages/localization.json';
import { NotEmptyValidatorDirective } from './validators/not-empty-validator.directive';

@NgModule({
    declarations: [
        AppComponent,
        NavbarComponent,
        ClientsComponent,
        ManufacturersComponent,
        ManufacturerGroupsComponent,
        PartNumbersComponent,
        IndexComponent,
        AdminsComponent,
        NotFoundComponent,
        InternalErrorComponent,
        NetworkErrorComponent,
        ForbiddenComponent,
        EmptyGridMessageComponent,
        FormErrorsComponent,

        CertCNValidatorDirective,
        MinValidatorDirective,
        ConditionalRequiredValidatorDirective,
        NotEmptyValidatorDirective
    ],
    imports: [
        FormsModule,
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        HttpClientModule,
        StorageServiceModule,
        BsDropdownModule.forRoot(),
        CollapseModule.forRoot(),
        ModalModule.forRoot(),
        BsDatepickerModule.forRoot(),
        TooltipModule.forRoot(),
        PaginationModule.forRoot(),
        ProgressbarModule.forRoot(),
        NgxCleaveDirectiveModule,
        NgSelectModule,
        NgxPrettyCheckboxModule
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: JsonDateInterceptor, multi: true }
    ],
    schemas: [
    ],
    bootstrap: [AppComponent]
})
export class AppModule {

    /**
     * Constructor.
     * Initializes locale for currency formatting and datepicker localization.
     * @param localeService BsLocaleService instance
     */
    constructor(localeService: BsLocaleService) {
        ruLocale.invalidDate = LOCALIZATION.data.invalidDate;
        defineLocale(environment.locale, ruLocale);
        registerLocaleData(localeRu);
        localeRu[ɵLocaleDataIndex.NumberSymbols][NumberSymbol.Decimal] = '.';
        localeService.use(environment.locale);
    }
 }
