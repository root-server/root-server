/**
 * Client interface.
 */
export interface Client {
    id?: number;
    certCN?: string;
    organizationName?: string;
    contactPersonFullName?: string;
    contactPersonPhone?: string;
    notificationEmail?: string;
    balance?: number;
    paidTill?: Date;
}
