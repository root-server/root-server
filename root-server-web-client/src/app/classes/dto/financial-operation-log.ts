import { Client } from './client';
import { FinancialOperationType } from './financial-operation-type';

/**
 * Financial operation log interface.
 */
export interface FinancialOperationLog {
    clientId: number;
    balanceChange?: number;
    paidTill?: Date;
    operationType?: FinancialOperationType;
    reason?: string;
}
