/**
 * Financial operation type interface.
 */
export interface FinancialOperationType {
    id?: number;
    name?: string;
    reasonRequired?: boolean;
}
