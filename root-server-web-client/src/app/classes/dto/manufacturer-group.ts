/**
 * Manufacturer group interface.
 */
export interface ManufacturerGroup {
    id?: number;
    name?: string;
    manufacturerNames?: string;
}
