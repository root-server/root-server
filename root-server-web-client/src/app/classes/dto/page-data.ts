/**
 * Represents object list paginated data.
 */
export interface PageData<T> {
    total?: number;
    data?: T[];
}
