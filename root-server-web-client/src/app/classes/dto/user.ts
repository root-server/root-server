/**
 * User interface.
 */
export interface User {
    id?: number;
    certCN?: string;
    active?: boolean;
}
