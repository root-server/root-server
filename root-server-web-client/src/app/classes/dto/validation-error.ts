/**
 * Server validation error interface.
 */
export interface ValidationError {
    objectName?: string;
    fieldName?: string;
    errorCode?: string;
    defaultMessage?: string;
}
