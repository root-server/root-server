import { NgForm, AbstractControl, NgModel } from '@angular/forms';
import { ValidationError } from '../dto/validation-error';
import { HttpErrorResponse } from '@angular/common/http';
import { EMPTY, Observable } from 'rxjs';
import * as VALIDATION_ERRORS from '../../messages/validation-errors.json';
import { NgSelectComponent } from '@ng-select/ng-select';

/** page size selected by default */
export const DEFAULT_PAGE_SIZE: number = 15;

/** availabe sizes for grid pagination */
export const AVAILABLE_PAGE_SIZES: number[] = [10, 15, 30, 50];

/** debounce time for pager and page size selector */
export const GRID_PAGER_DEBOUNCE_TIME: number = 50;

/**
 * Returns form control CSS class. If control in invalid state 'invalid-data' returnd, empty otherwise.
 * @param form NgForm instance
 * @param control NgModel instance
 */
export function getControlCssClass(form: NgForm, control: NgModel): string {
    const invalidClassName: string = 'invalid-data';
    const controlInvalid: boolean = (form.submitted || control.touched) && control.invalid;

    /* must set invalid class name to first child element of NgSelectComponent, not to ng-select itself */
    let skipClassSetup: boolean = false;
    if (control.valueAccessor instanceof NgSelectComponent) {
        let comp: NgSelectComponent = control.valueAccessor;
        if (controlInvalid) {
            comp.element.children[0].classList.add(invalidClassName);
        } else {
            comp.element.children[0].classList.remove(invalidClassName)
        }
        skipClassSetup = true;
    }

    return (controlInvalid && !skipClassSetup) ? invalidClassName : '';
}

/**
 * Returns description for client-side/server-side validation error.
 * @param ve ValidationError instance
 */
export function getValidationErrorDescription(ve: ValidationError): string {
    if (ve.fieldName) {
        let desc = VALIDATION_ERRORS.data[ve.objectName + '.' + ve.fieldName + '.' + ve.errorCode];
        if (!desc) {
            desc = VALIDATION_ERRORS.data['control.default'] + ' "' + ve.fieldName + '"';
        }
        return desc;
    } else {
        let desc = VALIDATION_ERRORS.data[ve.objectName + '.' + ve.errorCode];
        if (!desc) {
            desc = VALIDATION_ERRORS.data['form.default'];
        }
        return desc;
    }
}

/**
 * Handles server validation error, set form control error.
 * @param err HttpErrorResponse instance
 * @param form NgForm instance
 */
export function handleServerValidationError(err: HttpErrorResponse, form: NgForm): Observable<any> {
    if (err instanceof HttpErrorResponse && err.status == 422 && err.error) {
        /* server errors always represents array of ValidationError instances */
        err.error.forEach((ve: ValidationError) => {
            /* form control errors */
            if (ve.fieldName) {
                const formControl: AbstractControl = form.controls[ve.fieldName];
                if (formControl) {
                    addControlError(formControl, ve);
                } else {
                    /* errors with non existing control (set up manually in code) */
                    addFormError(form, ve);
                }
            } else {
                /* general errors */
                addFormError(form, ve);
            }
        })
    }
    return EMPTY;
}

/**
 * Adds validation error to form control.
 * @param control AbstractControl instance
 * @param validationError ValidationError instance
 */
function addControlError(control: AbstractControl, validationError: ValidationError): void {
    if (!control.errors) {
        control.setErrors({serverValidation: []});
    }
    control.errors.serverValidation.push(validationError);
}

/**
 * Adds validation error to form.
 * @param form NgForm instamce
 * @param validationError ValidationError instance
 */
function addFormError(form: NgForm, validationError: ValidationError): void {
    if (!form.errors) {
        form.form.setErrors({serverValidation : []})
    }
    form.form.errors.serverValidation.push(validationError);
}