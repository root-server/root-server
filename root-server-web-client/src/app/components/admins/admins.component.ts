import { Component, TemplateRef, ViewChild, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/classes/dto/user';
import { catchError, map } from 'rxjs/operators';
import { EMPTY } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { NgForm, NgModel } from '@angular/forms';
import { getControlCssClass, handleServerValidationError } from 'src/app/classes/util/root-server-util';

/**
 * Administrator list and management conmponent.
 */
@Component({
    selector: 'app-admins',
    templateUrl: './admins.component.html'
})
export class AdminsComponent implements OnInit {

    admins: User[];
    admin: User;

    manageAdminModalRef: BsModalRef;
    missingAdminModalRef: BsModalRef;

    private pkcs12Progress: number;

    @ViewChild('missingAdminTemplate', {static: true})
    private missingAdminTemplate: TemplateRef<any>;
    @ViewChild('generatePkcs12Template', {static: true})
    private generatePkcs12Template: TemplateRef<any>;

    /**
     * Constructor.
     * @param userService UserService instance
     */
    constructor(private userService: UserService,
                private modalService: BsModalService) {
    }

    /**
     * Opens modal dialog for new administrator addition or existing one editing.
     * @param template TemplateRef instance
     * @param adminId administrator identifier
     */
    openManageAdminModal(template: TemplateRef<any>, adminId: number): boolean {
        this.admin = null;

        /* arrow function, that opens manage admin modal on load admin callback or by direct call */
        let openManageAdminModalCallback =
            (template: TemplateRef<any>) => this.manageAdminModalRef = this.modalService.show(template, {backdrop: 'static'});

        if (adminId) {
            this.loadAdmin(adminId, () => openManageAdminModalCallback(template));
        } else {
            this.admin = {};
            openManageAdminModalCallback(template);
        }
        return false;
    }

    /**
     * Downloads generated PKCS12 container for particular administrator.
     * @param adminId administrator identifier
     */
    getAdminPkcs12(adminId: number) {
        this.pkcs12Progress = 0;
        const generatePkcs12TemplateRef = this.modalService.show(
            this.generatePkcs12Template, {keyboard: false, animated: false, backdrop: 'static'}
        );
        const progressTimerId = setInterval(
            () => this.pkcs12Progress += this.pkcs12Progress < 100 ? 20 : 0, 800
        );
        const closeGeneratePkcs12Modal = () => {
            clearInterval(progressTimerId);
            generatePkcs12TemplateRef.hide();
        };
        this.userService.getUserPkcs12(adminId).pipe(
                map((data) => {
                    closeGeneratePkcs12Modal();
                    saveAs(data, this.admins.find(admin => admin.id == adminId).certCN + ".p12");
                }),
                catchError((err) => {
                    closeGeneratePkcs12Modal();
                    if (err instanceof HttpErrorResponse && err.status == 410) {
                        setTimeout(
                            () => this.missingAdminModalRef = this.modalService.show(this.missingAdminTemplate, {class: 'modal-sm'}), 0
                        );
                    }
                    return EMPTY;
                })
            ).subscribe();
        return false;
    }

    /**
     * Saves administrator.
     * @param adminForm admin NgForm instance
     */
    saveAdmin(adminForm: NgForm): void {
        this.userService.saveUser(this.admin).pipe(
            catchError((err) => handleServerValidationError(err, adminForm))
        ).subscribe(() => {
            this.loadAdmins();
            this.manageAdminModalRef.hide();
        });
    }

    /**
     * Returns form control CSS class. If control in invalid state 'invalid-data' returnd, empty otherwise.
     * @param form NgForm instance
     * @param control NgModel instance
     */
    controlCssClass(form: NgForm, control: NgModel): string {
        return getControlCssClass(form, control);
    }

    /**
     * Initializes component.
     */
    ngOnInit(): void {
        this.loadAdmins();
    }

    /**
     * Loads administrator by identifier.
     * @param id administrator identifier
     */
    private loadAdmin(id: number, callback: () => void): void {
        this.userService.getUser(id)
            .pipe(
                catchError((err) => {
                    if (err instanceof HttpErrorResponse && err.status == 410) {
                        this.missingAdminModalRef = this.modalService.show(this.missingAdminTemplate, {class: 'modal-sm'});
                    }
                    return EMPTY;
                }),
                map((admin: User) => this.admin = admin)
            )
            .subscribe(() => callback());
    }

    /**
     * Loads list of administrators.
     */
    private loadAdmins(): void {
        this.userService.getUsers()
            .pipe(map((admins: User[]) => this.admins = admins))
            .subscribe();
    }
}
