import { Component, OnInit, TemplateRef, ViewChild  } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ClientService } from 'src/app/services/client.service';
import { ClassifierService } from 'src/app/services/classifier.service';
import { Client } from 'src/app/classes/dto/client';
import { FinancialOperationLog } from 'src/app/classes/dto/financial-operation-log';
import { getControlCssClass, handleServerValidationError, DEFAULT_PAGE_SIZE, AVAILABLE_PAGE_SIZES, GRID_PAGER_DEBOUNCE_TIME } from 'src/app/classes/util/root-server-util';
import { catchError, map, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { NgForm, NgModel } from '@angular/forms';
import { FinancialOperationType } from 'src/app/classes/dto/financial-operation-type';
import { PageData } from 'src/app/classes/dto/page-data';
import { Subject, of, EMPTY } from 'rxjs';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { HttpErrorResponse } from '@angular/common/http';
import { saveAs } from 'file-saver';

/**
 * Client list and management component.
 */
@Component({
    selector: 'app-clients',
    templateUrl: './clients.component.html'
})
export class ClientsComponent implements OnInit {

    clients: Client[];
    client: Client;
    financialOperationLog: FinancialOperationLog;
    financialOperationTypes: FinancialOperationType[];
    financialOperationsPage: PageData<FinancialOperationLog> = {};
    financialOperationPageNumber: number;
    financialOperationPageSize: number;
    financialOperationPageSizes: number[] = AVAILABLE_PAGE_SIZES;

    /** extra subject to debounce requests from pager and page size selector (simply wraps latest timestamp) */
    financialOperationPageChangeTs$: Subject<number>;

    manageClientModalRef: BsModalRef;
    editClientBalanceModalRef: BsModalRef;
    viewClientFinancialOperationsModalRef: BsModalRef;
    missingClientModalRef: BsModalRef;

    private financialOperationsTimer: any;
    private pkcs12Progress: number;

    @ViewChild('missingClientTemplate', {static: true})
    private missingClientTemplate: TemplateRef<any>;
    @ViewChild('generatePkcs12Template', {static: true})
    private generatePkcs12Template: TemplateRef<any>;

    private FINANCIAL_OPERATION_PAGE_SIZE_KEY: string = 'clients.financialOperations.pageSize';

    /**
     * Constructor.
     * @param clientService ClientService instance
     * @param classifierService ClassifierService instance
     * @param modalService BsModalService instance
     */
    constructor(private clientService: ClientService,
                private classifierService: ClassifierService,
                private localStorageService: LocalStorageService,
                private modalService: BsModalService) {
    }

    /**
     * Opens modal dialog for new client addition or existing one editing.
     * @param template TemplateRef instance
     * @param clientId client identifier
     */
    openManageClientModal(template: TemplateRef<any>, clientId: number): boolean {
        this.client = null;

        /* arrow function, that opens manage client modal on load client callback or by direct call */
        let openManageClientModalCallback =
            (template: TemplateRef<any>) => this.manageClientModalRef = this.modalService.show(template, {backdrop: 'static'});

        if (clientId) {
            this.loadClient(clientId, () => openManageClientModalCallback(template));
        } else {
            this.client = {};
            openManageClientModalCallback(template);
        }
        return false;
    }

    /**
     * Opens modal dialog for client balance change.
     * @param template TemplateRef instance
     * @param clientId client identifier
     */
    openEditClientBalanceModal(template: TemplateRef<any>, clientId: number): boolean {
        this.client = null;
        if (!this.financialOperationTypes) {
            this.loadFinancialOperationTypes();
        }

        /* arrow function, that opens edit client balance modal on load client callback */
        let openEditClientBalanceModalCallback = (template: TemplateRef<any>) => {
            this.financialOperationLog = {
                clientId: this.client.id,
                paidTill: this.client.paidTill
            };
            this.editClientBalanceModalRef = this.modalService.show(template, {backdrop: 'static'});
        };

        this.loadClient(clientId, () => openEditClientBalanceModalCallback(template));
        return false;
    }

    /**
     * Opens modal dialog to display client financial operations.
     * @param template TemplateRef instance
     * @param clientId client identifier
     */
    openViewClientFinancialOperationsModal(template: TemplateRef<any>, clientId: number): boolean {
        this.client = null;

        /* arrow function, that opens client financial operations modal on load client callback */
        let openViewClientFinancialOperationsModalCallback = (template: TemplateRef<any>) => {
            this.financialOperationPageNumber = 1;
            this.financialOperationPageSize = this.localStorageService.getNumber(this.FINANCIAL_OPERATION_PAGE_SIZE_KEY) || DEFAULT_PAGE_SIZE;
            this.financialOperationPageChangeTs$ = new Subject<number>();
            this.financialOperationPageChangeTs$.pipe(
                debounceTime(GRID_PAGER_DEBOUNCE_TIME),
                distinctUntilChanged(),
                switchMap((v) => {this.loadFinancialOperations(); return of(v);})
            ).subscribe();
            this.triggerFinancialOperationsReload();
            this.viewClientFinancialOperationsModalRef = this.modalService.show(template, {class: 'modal-xl'});
        };

        this.loadClient(clientId, () => openViewClientFinancialOperationsModalCallback(template));
        return false;
    }

    /**
     * Downloads generated PKCS12 container for particular client.
     * @param clientId client identifier
     */
    getClientPkcs12(clientId: number) {
        this.pkcs12Progress = 0;
        const generatePkcs12TemplateRef = this.modalService.show(
            this.generatePkcs12Template, {keyboard: false, animated: false, backdrop: 'static'}
        );
        const progressTimerId = setInterval(
            () => this.pkcs12Progress += this.pkcs12Progress < 100 ? 20 : 0, 800
        );
        const closeGeneratePkcs12Modal = () => {
            clearInterval(progressTimerId);
            generatePkcs12TemplateRef.hide();
        };
        this.clientService.getClientPkcs12(clientId).pipe(
                map((data) => {
                    closeGeneratePkcs12Modal();
                    saveAs(data, this.clients.find(client => client.id == clientId).certCN + ".p12");
                }),
                catchError((err) => {
                    closeGeneratePkcs12Modal();
                    if (err instanceof HttpErrorResponse && err.status == 410) {
                        setTimeout(
                            () => this.missingClientModalRef = this.modalService.show(this.missingClientTemplate, {class: 'modal-sm'}), 0
                        );
                    }
                    return EMPTY;
                })
            ).subscribe();
        return false;
    }

    /**
     * Saves client.
     * @param clientForm client NgForm instance
     */
    saveClient(clientForm: NgForm): void {
        this.clientService.saveClient(this.client).pipe(
            catchError((err) => handleServerValidationError(err, clientForm))
        ).subscribe(() => {
            this.loadClients();
            this.manageClientModalRef.hide();
        });
    }

    /**
     * Updates client balance.
     * @param clientBalanceForm client balance NgForm instance
     */
    updateClientBalance(clientBalanceForm: NgForm): void {
        const financialOperationLog = {
            ...this.financialOperationLog,
            balanceChange: parseFloat(String(this.financialOperationLog.balanceChange).replace(/ /g, ''))
        };
        this.clientService.changeClientBalance(financialOperationLog).pipe(
            catchError((err) => handleServerValidationError(err, clientBalanceForm))
        ).subscribe(() => {
            this.loadClients();
            this.editClientBalanceModalRef.hide();
        });
    }

    /**
     * Changes subject variable to trigger
     * financial operation list loading when user selects other page.
     */
    selectFinancialOperationsPage(): void {
        this.triggerFinancialOperationsReload();
    }

    /**
     * Resets grid page number and page size and changes subject variable to trigger
     * financial operation list loading when user changes page size.
     * @param pageSize page size
     */
    selectFinancialOperationsPageSize(pageSize: number): void {
        /** cannot setup first page number directly */
        this.localStorageService.setNumber(this.FINANCIAL_OPERATION_PAGE_SIZE_KEY, pageSize);
        this.financialOperationsTimer = setTimeout(() => this.financialOperationPageNumber = 1, 0);
        this.triggerFinancialOperationsReload();
    }

    /**
     * Returns form control CSS class. If control in invalid state 'invalid-data' returnd, empty otherwise.
     * @param form NgForm instance
     * @param control NgModel instance
     */
    controlCssClass(form: NgForm, control: NgModel): string {
        return getControlCssClass(form, control);
    }

    /**
     * Initializes component.
     */
    ngOnInit(): void {
        this.loadClients();
    }

    /**
     * Trigger event to force financial operation list reload.
     */
    private triggerFinancialOperationsReload(): void {
        this.financialOperationPageChangeTs$.next(new Date().getTime());
    }

    /**
     * Loads client by identifier.
     * @param id client identifier
     */
    private loadClient(id: number, callback: () => void): void {
        this.clientService.getClient(id)
            .pipe(
                catchError((err) => {
                    if (err instanceof HttpErrorResponse && err.status == 410) {
                        this.missingClientModalRef = this.modalService.show(this.missingClientTemplate, {class: 'modal-sm'});
                    }
                    return EMPTY;
                }),
                map((client: Client) => this.client = client)
            )
            .subscribe(() => callback());
    }

    /**
     * Loads list of clients.
     */
    private loadClients(): void {
        this.clientService.getClients()
            .pipe(map((clients: Client[]) => this.clients = clients))
            .subscribe();
    }

    /**
     * Loads list of financial operation types.
     */
    private loadFinancialOperationTypes(): void {
        this.classifierService.getFinancialOperationTypes()
            .pipe(map((response: FinancialOperationType[]) => this.financialOperationTypes = response))
            .subscribe();
    }

    /**
     * Loads financial operations page.
     */
    private loadFinancialOperations(): void {
        this.clientService.getClientFinancialOperations(this.client.id, this.financialOperationPageNumber, this.financialOperationPageSize)
            .pipe(map((response: PageData<FinancialOperationLog>) => this.financialOperationsPage = response))
            .subscribe(() => {
                 if (this.financialOperationsTimer) {
                    clearTimeout(this.financialOperationsTimer);
                }
            });
    }
}
