import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmptyGridMessageComponent } from './empty-grid-message.component';

describe('EmptyGridMessageComponent', () => {
  let component: EmptyGridMessageComponent;
  let fixture: ComponentFixture<EmptyGridMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmptyGridMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmptyGridMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
