import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-empty-grid-message',
    templateUrl: './empty-grid-message.component.html'
})
export class EmptyGridMessageComponent {

    @Input() message: string;
}
