import { Component, OnInit, Input } from '@angular/core';
import { NgForm, AbstractControl } from '@angular/forms';
import { ValidationError } from 'src/app/classes/dto/validation-error';
import { getValidationErrorDescription } from 'src/app/classes/util/root-server-util';

/**
 * Validation errors component.
 */
@Component({
    selector: 'app-form-errors',
    templateUrl: './form-errors.component.html'
})
export class FormErrorsComponent implements OnInit {

    @Input('form') form: NgForm;
    @Input('objectName') objectName: string;

    private ngErrorToCode: Map<string, string>;

    /**
     * Collects and returns validation errors.
     */
    get errors(): ValidationError[] {
        let result: ValidationError[] = [];

        /* control errors */
        Object.keys(this.form.controls)
              .filter(key => {
                    const ctrl = this.form.controls[key];
                    return (this.form.submitted || ctrl.touched) && ctrl.invalid;
               })
              .forEach(key => {
                    const ctrl: AbstractControl = this.form.controls[key];
                    const fieldName: string = Object.keys(this.form.controls).find(name => ctrl === this.form.controls[name]);
                    Object.keys(ctrl.errors).forEach(errKey => {
                        if (ctrl.errors[errKey] instanceof Array) {
                            result = result.concat(ctrl.errors[errKey]);
                        } else {
                            result.push({objectName: this.objectName, fieldName: fieldName, errorCode: this.ngErrorToCode.get(errKey)})
                        }
                    })
              });

        /* general errors: can be set only on server side */
        if (this.form.errors) {
            Object.keys(this.form.errors).forEach(errKey => {
                result = result.concat(this.form.errors[errKey]);
            });
        }
        return result;
    }

    /**
     * Returns true, if errors block must be shown, false otherwise.
     */
    get showErrors(): boolean {
        return (this.form.submitted && !!this.form.errors)
            || Object.keys(this.form.controls).some(key => {
                const ctrl = this.form.controls[key];
                return (this.form.submitted || ctrl.touched) && ctrl.invalid;
        });
    }

    /**
     * Returns description for client-side/server-side validation error.
     * @param ve ValidationError instance
     */
    getErrorDescription(ve: ValidationError): string {
        return getValidationErrorDescription(ve);
    }

    /**
     * Initializes map of angular error codes to message error codes (that couincides with server ones).
     */
    ngOnInit(): void {
        this.ngErrorToCode = new Map<string, string>();
        this.ngErrorToCode.set('required', 'NotNull');
        this.ngErrorToCode.set('not-empty', 'NotNull');
        this.ngErrorToCode.set('pattern', 'Pattern');
        this.ngErrorToCode.set('email', 'Email');
        this.ngErrorToCode.set('min', 'DecimalMin');
        this.ngErrorToCode.set('app-cert-cn', 'UniqueCertCn');
    }
}
