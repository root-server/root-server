import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ManufacturerGroupsComponent } from './manufacturer-groups.component';

describe('ManufacturerGroupsComponent', () => {
    let component: ManufacturerGroupsComponent;
    let fixture: ComponentFixture<ManufacturerGroupsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ManufacturerGroupsComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ManufacturerGroupsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
