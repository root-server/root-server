import { Component, OnInit } from '@angular/core';
import { ManufacturerService } from 'src/app/services/manufacturer.service';
import { ManufacturerGroup } from 'src/app/classes/dto/manufacturer-group'
import { map } from 'rxjs/operators';

/**
 * Manufacturer group list and management component.
 */
@Component({
    selector: 'app-manufacturer-groups',
    templateUrl: './manufacturer-groups.component.html'
})
export class ManufacturerGroupsComponent implements OnInit {

    groups: ManufacturerGroup[];

    /**
     * Constructor.
     * @param manufacturerService ManufacturerService instance
     */
    constructor(private manufacturerService: ManufacturerService) {
    }

    /**
     * Initializes component.
     */
    ngOnInit(): void {
        this.loadManufacturerGroups();
    }

    /**
     * Loads list of manufacturer groups.
     */
    private loadManufacturerGroups(): void {
        this.manufacturerService.getManufacturerGroups()
            .pipe(map((groups: ManufacturerGroup[]) => this.groups = groups))
            .subscribe();
    }
}
