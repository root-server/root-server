import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartNumbersComponent } from './part-numbers.component';

describe('PartNumbersComponent', () => {
  let component: PartNumbersComponent;
  let fixture: ComponentFixture<PartNumbersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartNumbersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartNumbersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
