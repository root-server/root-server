import { Injectable } from '@angular/core';
import {
    HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError, EMPTY } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

/**
 * Global HTTP error interceptor.
 */
@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

    /**
     * Constructor.
     * @param router Router instance
     */
    constructor(private router: Router) {}

    /**
     * Interceptor method.
     * @param req HttpRequest instance
     * @param next HttpHandler instance
     */
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            catchError((error) => {
                if (error instanceof HttpErrorResponse) {
                    switch (error.status) {
                        case 0:
                            this.router.navigateByUrl("/network-error");
                            break;
                        case 500:
                            this.router.navigateByUrl("/internal-error");
                            break;
                        case 403:
                            this.router.navigateByUrl("/forbidden");
                            break;
                        case 404:
                            this.router.navigateByUrl("/not-found");
                            break;
                        default:
                            /* other status codes must be handled manually */
                            return throwError(error);
                    }
                    return EMPTY;
                }
                return throwError(error);
            })
        );
    }
}
