import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

/**
 * Global HTTP interceptor, that converts JSON strings to Date objects from date ISO string.
 * Originates from https://gist.github.com/martinobordin/39bb1fe3400a29c1078dec00ff76bba9 and
 * https://medium.com/self-learning/ngx-datepicker-utc-datepicker-design-77e33789e9d7
 */
@Injectable()
export class JsonDateInterceptor implements HttpInterceptor {

    private iso8601: RegExp = /^\d{4}-\d{2}-\d{2}(T\d{2}:\d{2}:\d{2}(\.\d+)?(([+-]\d{2}:\d{2})|Z)?)?$/;

    /**
     * Interceptor method.
     * @param req HttpRequest instance
     * @param next HttpHandler instance
     */
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        req = req.clone({body: this.correctDates({...req.body})});
        return next.handle(req).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    const body = event.body;
                    this.convertToDate(body);
                }
                return event;
            })
        );
    }

    /**
     * Diminishes date value on timezone offset to save selected day when serializing.
     * @param body request body
     */
    private correctDates(body: any): any {
        if (body === null || body === undefined) {
            return body;
        }

        if (typeof body !== 'object') {
            return body;
        }

        for (const key of Object.keys(body)) {
            const value = body[key];
            if (value instanceof Date) {
                body[key] = new Date(value.getTime() - value.getTimezoneOffset() * 60 * 1000);
            } else if (typeof value === 'object') {
                this.correctDates(value);
            }
        }
        return body;
    }


    /**
     * Converts response body with dates as strings to dates as Date objects.
     * @param body response body
     */
    private convertToDate(body: any): void {
        if (body === null || body === undefined) {
            return;
        }

        if (typeof body !== 'object') {
            return;
        }

        for (const key of Object.keys(body)) {
            const value = body[key];
            if (this.isIso8601(value)) {
                body[key] = new Date(value);
            } else if (typeof value === 'object') {
                this.convertToDate(value);
            }
        }
    }

    /**
     * Tests passed value, whether it matches ISO date regular expression.
     * @param value value
     */
    private isIso8601(value: any): boolean {
        if (value === null || value === undefined) {
            return false;
        }
        return this.iso8601.test(value);
    }
}
