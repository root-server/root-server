import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { FinancialOperationType } from '../classes/dto/financial-operation-type';

/**
 * Classifier service methods collection.
 */
@Injectable({
    providedIn: 'root'
})
export class ClassifierService {

    private baseUrl = environment.baseUrl;

    /**
     * Constructor.
     * @param http HttpClient instance
     */
    constructor(private http: HttpClient) { }

    /**
     * Returns list of financial operation types.
     */
    getFinancialOperationTypes(): Observable<FinancialOperationType[]> {
        return this.http.get<FinancialOperationType[]>(this.baseUrl + `classifier/financial-operation-types`);
    }
}
