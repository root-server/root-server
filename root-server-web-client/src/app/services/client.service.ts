import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Client } from '../classes/dto/client';
import { FinancialOperationLog } from '../classes/dto/financial-operation-log';
import { PageData } from '../classes/dto/page-data';

/**
 * Client service methods collection.
 */
@Injectable({
    providedIn: 'root'
})
export class ClientService {

    private baseUrl = environment.baseUrl;

    /**
     * Constructor.
     * @param http HttpClient instance
     */
    constructor(private http: HttpClient) { }

    /**
     * Returns client, found by identifier.
     * @param id client identifier
     */
    getClient(id: number): Observable<Client> {
        return this.http.get<Client>(this.baseUrl + `clients/${id}`);
    }

    /**
     * Returns list of clients.
     */
    getClients(): Observable<Client[]> {
        return this.http.get<Client[]>(this.baseUrl + `clients`);
    }

    /**
     * Returns true, if client with passed certCN already exists, false otherwise.
     * @param id client identifier
     * @param certCN client certificate CN data
     */
    isClientExists(id: number, certCN: string): Observable<boolean> {
        return this.http.get<boolean>(
            this.baseUrl + `clients/exists/${certCN}/${id ? id : ''}`
        );
    }

    /**
     * Creates new or updates existing client.
     * @param client Client instance
     */
    saveClient(client: Client): Observable<Client | null> {
        if (client.id) {
            return this.http.put<Client>(this.baseUrl + `clients/update/${client.id}`, client);
        } else {
            return this.http.post<null>(this.baseUrl + `clients/create`, client);
        }
    }

    /**
     * Updates client balance.
     * @param financialOperationLog FinancialOperationLog instance
     */
    changeClientBalance(financialOperationLog: FinancialOperationLog): Observable<any> {
        return this.http.put(this.baseUrl + `clients/change-balance/${financialOperationLog.clientId}`, financialOperationLog);
    }

    /**
     * Returns list of client financial operations.
     * @param clientId climent identifier
     * @param pageNumber financial operations page number
     * @param pageSize page size
     */
    getClientFinancialOperations(clientId: number, pageNumber: number = 1, pageSize: number): Observable<PageData<FinancialOperationLog>> {
        let params = new HttpParams().set('page', pageNumber.toString()).set('pageSize', pageSize.toString());
        return this.http.get<PageData<FinancialOperationLog>>(
            this.baseUrl + `clients/financial-operations/${clientId}`, {params}
        );
    }

    /**
     * Returns generated PKCS12 container for particular client.
     * @param clientId client identifier
     */
    getClientPkcs12(clientId: number): Observable<Blob> {
        return this.http.get(this.baseUrl + `clients/certificate/${clientId}`, {responseType: 'blob'});
    }
}
