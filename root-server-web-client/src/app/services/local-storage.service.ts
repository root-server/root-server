import { Injectable, Inject } from '@angular/core';
import { LOCAL_STORAGE, StorageService, StorageTranscoder, StorageDecoder, StorageTranscoders, StorageEncoder } from 'ngx-webstorage-service';

/**
 * Local storage service.
 */
@Injectable({
    providedIn: 'root'
})
export class LocalStorageService {

    constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) { }

    /**
     * Stores key/value pair to local storage.
     * Value is assumed to be numeric type.
     * @param key key
     * @param value value
     */
    setNumber(key: string, value: any): void {
        this.set(key, value, StorageTranscoders.NUMBER);
    }

    /**
     * Retrieves value from local storage found by key.
     * Value is assumed to be numeric type.
     * @param key key
     */
    getNumber(key: string): number {
        return this.get(key, StorageTranscoders.NUMBER);
    }

    /**
     * Removes value from local storage with particular key.
     * @param key key
     */
    remove(key: string): void {
        this.storage.remove(key);
    }

    /**
     * Stores key/value pair to local storage.
     * @param key key
     * @param value value
     * @param encoder StorageEncoder instance
     */
    private set(key: string, value: any, encoder: StorageEncoder<any>): void {
        this.storage.set(key, value, encoder);
    }

    /**
     * Retrieves value from local storage found by key.
     * @param key key
     * @param decoder StorageDecoder instance
     */
    private get(key: string, decoder: StorageDecoder<any>): any {
        return this.storage.get(key, decoder);
    }
}
