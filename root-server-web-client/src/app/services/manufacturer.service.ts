import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { ManufacturerGroup } from '../classes/dto/manufacturer-group';

/**
 * Manufacturer service methods collection.
 */
@Injectable({
    providedIn: 'root'
})
export class ManufacturerService {

    private baseUrl = environment.baseUrl;

    /**
     * Constructor.
     * @param http HttpClient instance
     */
    constructor(private http: HttpClient) { }

    /**
     * Returns observable of manufacturer groups list.
     */
    public getManufacturerGroups(): Observable<ManufacturerGroup[]> {
        return this.http.get<ManufacturerGroup[]>(this.baseUrl + `manufacturer/groups`);
    }
}
