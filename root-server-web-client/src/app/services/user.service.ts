import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { User } from '../classes/dto/user';

/**
 * User service.
 */
@Injectable({
    providedIn: 'root'
})
export class UserService {

    private baseUrl = environment.baseUrl;

    /**
     * Constructor.
     * @param http HttpClient instance
     */
    constructor(private http: HttpClient) { }

    /**
     * Returns observable of currently logged user.
     */
    public getCurrentUser(): Observable<User> {
        return this.http.get<User>(this.baseUrl + `users/current`);
    }

    /**
     * Returns user, found by identifier.
     * @param id user identifier
     */
    getUser(id: number): Observable<User> {
        return this.http.get<User>(this.baseUrl + `users/${id}`);
    }

    /**
     * Returns list of users.
     */
    getUsers(): Observable<User[]> {
        return this.http.get<User[]>(this.baseUrl + `users`);
    }

    /**
     * Returns true, if user with passed certCN already exists, false otherwise.
     * @param id user identifier
     * @param certCN user certificate CN data
     */
    isUserExists(id: number, certCN: string): Observable<boolean> {
        return this.http.get<boolean>(
            this.baseUrl + `users/exists/${certCN}/${id ? id : ''}`
        );
    }

    /**
     * Creates new or updates existing user.
     * @param user User instance
     */
    saveUser(user: User): Observable<User | null> {
        if (user.id) {
            return this.http.put<User>(this.baseUrl + `users/update/${user.id}`, user);
        } else {
            return this.http.post<null>(this.baseUrl + `users/create`, user);
        }
    }

    /**
     * Returns generated PKCS12 container for particular user.
     * @param userId user identifier
     */
    getUserPkcs12(userId: number): Observable<Blob> {
        return this.http.get(this.baseUrl + `users/certificate/${userId}`, {responseType: 'blob'});
    }
}

