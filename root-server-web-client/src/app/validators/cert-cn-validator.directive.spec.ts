import { CertCNValidatorDirective } from './cert-cn-validator.directive';
import { TestBed } from '@angular/core/testing';
import { ClientService } from '../services/client.service';

describe('CertCNValidatorDirective', () => {
    it('should create an instance', () => {
        const service: ClientService = TestBed.get(ClientService);
        const directive = new CertCNValidatorDirective(service);
        expect(directive).toBeTruthy();
    });
});
