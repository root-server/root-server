import { Directive, Input } from '@angular/core';
import { AbstractControl, ValidationErrors, NG_ASYNC_VALIDATORS, AsyncValidator } from '@angular/forms';
import { ClientService } from '../services/client.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserService } from '../services/user.service';

/**
 * Client certificate CN RDN (login) validation directive.
 */
@Directive({
    selector: '[app-cert-cn]',
    providers: [{
        provide: NG_ASYNC_VALIDATORS,
        useExisting: CertCNValidatorDirective,
        multi: true
    }]
})
export class CertCNValidatorDirective implements AsyncValidator {

    @Input('app-cert-cn') clientId: number;
    @Input('app-cert-cn-type') type: string;

    private handlers: Map<string, (control: AbstractControl) => Observable<ValidationErrors | null>> = new Map();

    /**
     * Constructor.
     * @param userService UserService instance
     * @param clientService ClientService instance
     */
    constructor(private userService: UserService,
                private clientService: ClientService) {
        let duplicateFn = (result: Observable<boolean>) => result.pipe(map(duplicate => duplicate ? {'app-cert-cn': true} : null));
        this.handlers.set(
            'client',
            (control: AbstractControl) =>
                duplicateFn(this.clientService.isClientExists(this.clientId, control.value))
        );
        this.handlers.set(
            'user',
            (control: AbstractControl) =>
                duplicateFn(this.userService.isUserExists(this.clientId, control.value))
        );
    }

    /**
     * Performs client certificate CN data validation.
     * @param control AbstractControl instance
     */
    validate(control: AbstractControl): Observable<ValidationErrors | null> {
        return this.handlers.get(this.type)(control);
    }
}
