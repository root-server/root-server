import { Directive, Input, SimpleChanges, OnChanges } from '@angular/core';
import { Validator, AbstractControl, ValidationErrors, Validators, NG_VALIDATORS } from '@angular/forms';

/**
 * Conditional required validation directive.
 */
@Directive({
    selector: '[app-conditional-required]',
    providers: [{
        provide: NG_VALIDATORS,
        useExisting: ConditionalRequiredValidatorDirective,
        multi: true
    }]
})
export class ConditionalRequiredValidatorDirective implements Validator, OnChanges {

    @Input('app-conditional-required') condition: any;

    private _onChange: () => void;

    /**
     * Performs required value validation on condition.
     * @param control AbstractControl instance
     */
    validate(control: AbstractControl): ValidationErrors | null {
        if (this.condition) {
            return Validators.required(control);
        }
        return null;
    }

    /**
     * Registers a callback function to call when validator input changes.
     * @param fn callback functions
     */
    registerOnValidatorChange(fn: () => void): void {
        this._onChange = fn;
    }

    /**
     * Calls validation on input change.
     * @param changes SimpleChanges instance
     */
    ngOnChanges(changes: SimpleChanges): void {
        if ('condition' in changes && this._onChange) {
            this._onChange();
        }
    }
}
