import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl, Validators, ValidationErrors } from '@angular/forms';

/**
 * Numeric min value validator directive.
 */
@Directive({
    selector: '[min]',
    providers: [{
        provide: NG_VALIDATORS,
        useExisting: MinValidatorDirective,
        multi: true
    }]
})
export class MinValidatorDirective implements Validator {

    @Input() min: number;

    /**
     * Performs min value validation.
     * @param control AbstractControl instance
     */
    validate(control: AbstractControl): ValidationErrors | null {
        return Validators.min(this.min)(control);
    }
}
