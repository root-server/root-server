import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl, ValidationErrors, Validators } from '@angular/forms';

/**
 * String value not empty validator directive.
 */
@Directive({
    selector: '[not-empty]',
    providers: [{
      provide: NG_VALIDATORS,
      useExisting: NotEmptyValidatorDirective,
      multi: true
  }]
})
export class NotEmptyValidatorDirective implements Validator {

    /**
     * Performs not mpty value validation.
     * @param control AbstractControl instance
     */
    validate(control: AbstractControl): ValidationErrors | null {
        return !!control.value && control.value.trim().length == 0 ? {'not-empty': null} : null;
    }
}
