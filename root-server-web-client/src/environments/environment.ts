export const environment = {
    production: false,
    baseUrl: 'https://root.server:8444/rest/',
    locale: 'ru'
};
