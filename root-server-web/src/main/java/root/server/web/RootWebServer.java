package root.server.web;

import java.io.IOException;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.deser.std.StdScalarDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;

import root.server.dao.config.RootServerDaoConfiguration;
import root.server.web.annotation.RootServerRestController;
import root.server.web.security.RootWebServerUserDetailsService;

/**
 * Spring boot application for administrator web panel.
 * @author Alexey Chalov
 */
@EnableWebSecurity
@SpringBootApplication
@EnableTransactionManagement
@Import(RootServerDaoConfiguration.class)
@PropertySource(value = {"classpath:ValidationMessages.properties"}, encoding = "UTF-8")
public class RootWebServer extends WebSecurityConfigurerAdapter {

    @Autowired
    private RootWebServerUserDetailsService userDetailsService;

    /**
     * Starts administrator web panel.
     * @param args program arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(RootWebServer.class, args);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
            .antMatchers("/error/**").permitAll()
            .anyRequest().authenticated()
            .and().csrf().disable().cors()
            .and().x509().userDetailsService(userDetailsService);
    }

    /**
     * Registers new Jackson module, that forces deserializer to trim input string values.
     * @return {@link Module} instance
     */
    @Bean
    @SuppressWarnings("serial")
    Module registerStringTrimmerModule() {
        return new SimpleModule() {{
            addDeserializer(String.class, new StdScalarDeserializer<String>(String.class) {

                @Override
                public String deserialize(JsonParser jsonParser, DeserializationContext ctx)
                                                                 throws IOException, JsonProcessingException {
                    return jsonParser.getValueAsString().trim();
                }
            });
        }};
    }

    /**
     * Allows CORS for all server paths.
     * @return {@link CorsConfigurationSource} instance
     */
    @Bean
    @Profile("dev")
    CorsConfigurationSource corsConfigurationSource() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration corsConfiguration = new CorsConfiguration().applyPermitDefaultValues();
        corsConfiguration.addAllowedMethod(HttpMethod.PUT);
        /* Angular Live Development Server */
        corsConfiguration.setAllowedOrigins(Arrays.asList(new String[] {"http://localhost:4200"}));
        source.registerCorsConfiguration(RootServerRestController.BASE_PATH + "/**", corsConfiguration);
        return source;
    }
}
