package root.server.web.annotation;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Base annotation for module rest controllers, that holds their base path.
 * @author Alexey Chalov
 */
@Target(TYPE)
@Retention(RUNTIME)
@RestController
@RequestMapping(RootServerRestController.BASE_PATH)
public @interface RootServerRestController {

    String BASE_PATH = "/rest";
}
