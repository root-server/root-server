package root.server.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * SPA controller that forwards all requests to SPA application.
 * @author Alexey Chalov
 */
@Controller
public class Html5PathsController {

    /**
     * Forwards all requests to index page, cause it is SPA application responsibility to handle paths.
     * @return forward string
     */
    @RequestMapping(value = "/{[path:[^\\.]*}")
    public String redirect() {
        return "forward:/index.html";
    }
}
