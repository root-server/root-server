package root.server.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;

import root.server.common.entity.FinancialOperationType;
import root.server.dao.ClassifierDao;
import root.server.web.annotation.RootServerRestController;

/**
 * Classifier service methods collection.
 * @author Alexey Chalov
 */
@RootServerRestController
public class ClassifierService {

    @Autowired
    private ClassifierDao classifierDao;

    private static final String PATH = "/classifier";

    /**
     * Returns list of financial operation types visible on UI.
     * @return list of financial operation types visible on UI
     */
    @GetMapping(PATH + "/financial-operation-types")
    public List<FinancialOperationType> getVisibleFinancialOperationTypes() {
        return classifierDao.getVisibleFinancialOperationTypes();
    }
}
