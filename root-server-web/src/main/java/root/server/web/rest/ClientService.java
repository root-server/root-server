package root.server.web.rest;

import static org.springframework.http.ResponseEntity.created;
import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.http.ResponseEntity.ok;
import static root.server.web.util.RootServerUtil.getCurrentUserLogin;

import java.io.ByteArrayInputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import root.server.common.entity.Client;
import root.server.common.entity.FinancialOperationLog;
import root.server.common.entity.User;
import root.server.common.entity.base.PageData;
import root.server.common.validation.CorrectId;
import root.server.dao.ClassifierDao;
import root.server.dao.ClientDao;
import root.server.web.annotation.RootServerRestController;
import root.server.web.rest.exception.InvalidMethodArgumentException;
import root.server.web.service.crypto.CaType;
import root.server.web.service.crypto.CertificateGenerator;

/**
 * Client service methods collection.
 * @author Alexey Chalov
 */
@RootServerRestController
public class ClientService {

    @Autowired
    private ClientDao clientDao;
    @Autowired
    private ClassifierDao classifierDao;
    @Autowired
    private CertificateGenerator certificateGenerator;

    @Value("${root.server.validation.client.certCN.UniqueCertCn.message}")
    private String uniqueCertCnMessage;
    @Value("${root.server.validation.client.CorrectId.message}")
    private String clientCorrectIdMessage;
    @Value("${root.server.validation.financialOperationLog.client.CorrectId.message}")
    private String folClientCorrectIdMessage;
    @Value("${root.server.validation.financialOperationLog.reason.NotNull.message}")
    private String folReasonRequiredMessage;

    private static final String PATH = "/clients";

    /**
     * Returns full list of clients.
     * @return full list of clients
     */
    @GetMapping(PATH)
    public List<Client> getClients() {
        return clientDao.getClients();
    }

    /**
     * Returns client found by identifier.
     * @param clientId client identifier
     * @return {@link Client} instance
     */
    @GetMapping(PATH + "/{clientId}")
    public ResponseEntity<?> getClient(@PathVariable("clientId") Long clientId) {
        Client client = clientDao.getById(clientId);
        if (client == null) {
            return ResponseEntity.status(HttpStatus.GONE).build();
        }
        return ok(client);
    }

    /**
     * Returns true, if client with passed <code>certCN</code> already exists.
     * @param certCN client certificate CN RDN
     * @param clientId client identifier (will not be passed, if client creation is assumed)
     * @return boolean
     */
    @GetMapping({PATH + "/exists/{certCN}", PATH + "/exists/{certCN}/{clientId}"})
    public boolean isClientExists(@PathVariable("certCN") String certCN,
                                  @PathVariable(name = "clientId") Optional<Long> clientId) {
        return clientDao.isClientWithCertCnExists(clientId.orElse(null), certCN);
    }

    /**
     * Creates client.
     * @param client {@link Client} instance
     * @return {@link ResponseEntity} instance
     * @throws URISyntaxException must not happen
     */
    @PostMapping(PATH + "/create")
    public ResponseEntity<?> createClient(@Valid @RequestBody Client client) throws URISyntaxException {
        validateClientCertCn(null, client.getCertCN());
        clientDao.createClient(client);
        return created(new URI(RootServerRestController.BASE_PATH + PATH + "/" + client.getId())).body(client);
    }

    /**
     * Updates client.
     * @param clientId client identifier
     * @param client {@link Client} instance
     * @return {@link ResponseEntity} instance
     */
    @PutMapping(PATH + "/update/{clientId}")
    public ResponseEntity<?> updateClient(@PathVariable("clientId") Long clientId,
                                          @Valid @RequestBody Client client) {
        if (!clientDao.isClientWithIdExists(clientId)) {
            throw new InvalidMethodArgumentException(
                "client", null, CorrectId.class.getSimpleName(), clientCorrectIdMessage
            );
        }
        validateClientCertCn(clientId, client.getCertCN());
        client.setId(clientId);
        clientDao.updateClient(client);
        return noContent().build();
    }

    /**
     * Changes client balance, writes financial operation log.
     * @param clientId client identifier
     * @param log {@link FinancialOperationLog} instance
     * @return {@link ResponseEntity} instance
     */
    @PutMapping(PATH + "/change-balance/{clientId}")
    public ResponseEntity<?> changeClientBalance(@PathVariable("clientId") Long clientId,
                                                 @Valid @RequestBody FinancialOperationLog log) {
        if (!clientDao.isClientWithIdExists(clientId)) {
            throw new InvalidMethodArgumentException(
                "financialOperationLog", "client", CorrectId.class.getSimpleName(), folClientCorrectIdMessage
            );
        }
        if ((log.getReason() == null || log.getReason().isBlank())
              && classifierDao.getVisibleFinancialOperationType(log.getOperationType().getId()).isReasonRequired()) {
            throw new InvalidMethodArgumentException(
                "financialOperationLog", "reason", NotNull.class.getSimpleName(), folReasonRequiredMessage
            );
        }
        log.setPerformer(new User());
        log.getPerformer().setCertCN(getCurrentUserLogin());
        clientDao.changeClientBalance(clientId, log);
        return noContent().build();
    }

    /**
     * Returns list of client financial operations.
     * @param clientId client identifier
     * @param page page number
     * @param pageSize page size
     * @return list of client financial operations
     */
    @GetMapping(PATH + "/financial-operations/{clientId}")
    public ResponseEntity<?> getFinancialOperations(@PathVariable("clientId") Long clientId,
                                                    @RequestParam int page, @RequestParam int pageSize) {
        if (!clientDao.isClientWithIdExists(clientId)) {
            return ResponseEntity.status(HttpStatus.GONE).build();
        }
        return ok(new PageData<FinancialOperationLog>(
            clientDao.getFinancialOperationCount(clientId),
            clientDao.getFinancialOperations(clientId, (page - 1) * pageSize, pageSize)
        ));
    }

    /**
     * Returns input stream for client private key and certificate in PCKS12 format.
     * @param clientId client identifier
     * @return input stream
     */
    @GetMapping(PATH + "/certificate/{clientId}")
    public ResponseEntity<?> getCertificate(@PathVariable("clientId") Long clientId) {
        Client client = clientDao.getById(clientId);
        if (client == null) {
            return ResponseEntity.status(HttpStatus.GONE).build();
        }
        byte[] result = certificateGenerator.createPkcs12Store(client.getCertCN(), CaType.CLIENT);
        return ResponseEntity.status(HttpStatus.OK).contentLength(result.length)
                             .contentType(new MediaType("application", "x-pkcs12"))
                             .header("Content-Disposition", "attachment; fileName=\"" + client.getCertCN() + ".p12\"")
                             .body(new InputStreamResource(new ByteArrayInputStream(result)));
    }

    /**
     * Validates client certificate CN RDN on duplicate value.
     * @param id client identifier
     * @param certCN certificate CN
     */
    private void validateClientCertCn(Long id, String certCN) {
        if (clientDao.isClientWithCertCnExists(id, certCN)) {
            throw new InvalidMethodArgumentException("client", "certCN", "UniqueCertCn", uniqueCertCnMessage);
        }
    }
}
