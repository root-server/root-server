package root.server.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import root.server.common.entity.ManufacturerGroup;
import root.server.dao.ManufacturerDao;
import root.server.web.annotation.RootServerRestController;

/**
 * Manufacturer service methods collection.
 * @author Alexey Chalov
 */
@RootServerRestController
public class ManufacturerService {

    @Autowired
    private ManufacturerDao manufacturerDao;

    private static final String PATH = "/manufacturer";

    /**
     * Returns list of manufacturer groups.
     * @return list of manufacturer groups
     */
    @RequestMapping(PATH + "/groups")
    public List<ManufacturerGroup> getGroups() {
        return manufacturerDao.getGroups();
    }
}
