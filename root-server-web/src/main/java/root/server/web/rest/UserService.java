package root.server.web.rest;

import static org.springframework.http.ResponseEntity.created;
import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.http.ResponseEntity.ok;
import static root.server.web.util.RootServerUtil.getCurrentUserLogin;

import java.io.ByteArrayInputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import root.server.common.entity.User;
import root.server.common.validation.CorrectId;
import root.server.dao.UserDao;
import root.server.web.annotation.RootServerRestController;
import root.server.web.rest.exception.InvalidMethodArgumentException;
import root.server.web.service.crypto.CaType;
import root.server.web.service.crypto.CertificateGenerator;

/**
 * User service methods collection.
 * @author Alexey Chalov
 */
@RootServerRestController
public class UserService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private CertificateGenerator certificateGenerator;

    @Value("${root.server.validation.user.certCN.UniqueCertCn.message}")
    private String uniqueCertCnMessage;
    @Value("${root.server.validation.user.CorrectId.message}")
    private String userCorrectIdMessage;

    private static final String PATH = "/users";

    /**
     * Returns full list of users.
     * @return full list of users
     */
    @GetMapping(PATH)
    public List<User> getUsers() {
        return userDao.getUsers();
    }

    /**
     * Returns user found by identifier.
     * @param userId user identifier
     * @return {@link User} instance
     */
    @GetMapping(PATH + "/{userId}")
    public ResponseEntity<?> getUser(@PathVariable("userId") Long userId) {
        User user = userDao.getById(userId);
        if (user == null) {
            return ResponseEntity.status(HttpStatus.GONE).build();
        }
        return ok(user);
    }

    /**
     * Returns true, if with passed <code>certCN</code> already exists.
     * @param certCN user certificate CN RDN
     * @param userId user identifier (will not be passed, if user creation is assumed)
     * @return boolean
     */
    @GetMapping({PATH + "/exists/{certCN}", PATH + "/exists/{certCN}/{userId}"})
    public boolean isUserExists(@PathVariable("certCN") String certCN,
                                @PathVariable(name = "userId") Optional<Long> userId) {
        return userDao.isUserWithCertCnExists(userId.orElse(null), certCN);
    }

    /**
     * Returns current user.
     * @return current user
     */
    @GetMapping(PATH + "/current")
    public User getCurrentUser() {
        return userDao.getByCN(getCurrentUserLogin());
    }

    /**
     * Creates user.
     * @param user {@link User} instance
     * @return {@link ResponseEntity} instance
     * @throws URISyntaxException must not happen
     */
    @PostMapping(PATH + "/create")
    public ResponseEntity<?> createUser(@Valid @RequestBody User user) throws URISyntaxException {
        validateUserCertCn(null, user.getCertCN());
        userDao.createUser(user);
        return created(new URI(RootServerRestController.BASE_PATH + PATH + "/" + user.getId())).body(user);
    }

    /**
     * Updates user.
     * @param userId user identifier
     * @param user {@link User} instance
     * @return {@link ResponseEntity} instance
     */
    @PutMapping(PATH + "/update/{userId}")
    public ResponseEntity<?> updateUser(@PathVariable("userId") Long userId,
                                        @Valid @RequestBody User user) {
        if (!userDao.isUserWithIdExists(userId)) {
            throw new InvalidMethodArgumentException(
                "user", null, CorrectId.class.getSimpleName(), userCorrectIdMessage
            );
        }
        validateUserCertCn(user.getId(), user.getCertCN());
        user.setId(userId);
        userDao.updateUser(user);
        return noContent().build();
    }

    /**
     * Returns input stream for user private key and certificate in PCKS12 format.
     * @param userId user identifier
     * @return input stream
     */
    @GetMapping(PATH + "/certificate/{userId}")
    public ResponseEntity<?> getCertificate(@PathVariable("userId") Long userId) {
        User user = userDao.getById(userId);
        if (user == null) {
            return ResponseEntity.status(HttpStatus.GONE).build();
        }
        byte[] result = certificateGenerator.createPkcs12Store(user.getCertCN(), CaType.USER);
        return ResponseEntity.status(HttpStatus.OK).contentLength(result.length)
                             .contentType(new MediaType("application", "x-pkcs12"))
                             .header("Content-Disposition", "attachment; fileName=\"" + user.getCertCN() + ".p12\"")
                             .body(new InputStreamResource(new ByteArrayInputStream(result)));
    }

    /**
     * Validates user certificate CN RDN on duplicate value.
     * @param id user identifier
     * @param certCN certificate CN
     */
    private void validateUserCertCn(Long id, String certCN) {
        if (userDao.isUserWithCertCnExists(id, certCN)) {
            throw new InvalidMethodArgumentException("user", "certCN", "UniqueCertCn", uniqueCertCnMessage);
        }
    }
}
