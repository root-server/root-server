package root.server.web.rest.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Validation exception. Used by exception handler to correctly show it on UI.
 * @author Alexey Chalov
 */
@Getter
@AllArgsConstructor
@SuppressWarnings("serial")
public class InvalidMethodArgumentException extends RuntimeException {

    private final String objectName;
    private final String fieldName;
    private final String errorCode;
    private final String defaultMessage;
}
