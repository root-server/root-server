package root.server.web.rest.exception;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.context.MessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import lombok.extern.log4j.Log4j2;
import root.server.common.validation.CorrectId;
import root.server.common.validation.ValidationError;

/**
 * General exception handler.
 * @author Alexey Chalov
 */
@Log4j2
@RestControllerAdvice
public class RootServerExceptionHandler extends ResponseEntityExceptionHandler {

    private Map<String, Function<ObjectError, String>> objectErrorToField;

    /**
     * Internal server error exception handler.
     * @return {@link ResponseEntity} instance
     * @param t {@link Throwable} instance
     */
    @ExceptionHandler(Throwable.class)
    public ResponseEntity<?> internalServerError(Throwable t) {
        log.warn(t.getMessage(), t);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();

    }

    /**
     * Access denied exception handler.
     * @return {@link ResponseEntity} instance
     */
    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<?> accessDenied() {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    /**
     * Custom validation logic exception handler.
     * @param e {@link InvalidMethodArgumentException} instance
     * @return {@link ResponseEntity} instance
     */
    @ExceptionHandler(InvalidMethodArgumentException.class)
    public ResponseEntity<List<ValidationError>> invalidMethodArgument(InvalidMethodArgumentException e) {
        return ResponseEntity.unprocessableEntity().body(
            Arrays.asList(new ValidationError[] {
                new ValidationError(e.getObjectName(), e.getFieldName(), e.getErrorCode(), e.getDefaultMessage())
            })
        );
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException e,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status, WebRequest request) {
        return ResponseEntity.unprocessableEntity().body(
            e.getBindingResult().getAllErrors().stream()
             .map(fe -> new ValidationError(fe.getObjectName(), detectField(fe), fe.getCode(), fe.getDefaultMessage()))
             .collect(Collectors.toList())
         );
    }

    /**
     * Initializes map of error code to function, that detects field from {@link ObjectError} instance.
     */
    @PostConstruct
    void initialize() {
        objectErrorToField = new HashMap<String, Function<ObjectError, String>>();
        /* see SpringValidatorAdapter#getArgumentsForConstraint - last argument is CorrectId annotation fieldName*/
        objectErrorToField.put(
            CorrectId.class.getSimpleName(),
            (oe) -> ((MessageSourceResolvable) oe.getArguments()[oe.getArguments().length - 1]).getDefaultMessage()
        );
    }

    /**
     * Returns invalid field from {@link ObjectError} instance.
     * @param oe {@link ObjectError} instance
     * @return invalid field name
     */
    private String detectField(ObjectError oe) {
        if (oe instanceof FieldError) {
            return ((FieldError) oe).getField();
        }
        Function<ObjectError, String> func = objectErrorToField.get(oe.getCode());
        if (func == null) {
            throw new IllegalArgumentException("Unexpected object error with code: '" + oe.getCode() + "'.");
        }
        return func.apply(oe);
    }
}
