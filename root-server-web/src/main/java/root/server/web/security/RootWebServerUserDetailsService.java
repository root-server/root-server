package root.server.web.security;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import lombok.extern.log4j.Log4j2;
import root.server.common.entity.User;
import root.server.dao.UserDao;

/**
 * {@link UserDetailsService} implementation.
 * @author Alexey Chalov
 */
@Log4j2
@Service
public class RootWebServerUserDetailsService implements UserDetailsService {

    @Autowired
    private UserDao userDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userDao.getByCN(username);
        if (user == null) {
            throw new UsernameNotFoundException("No user with CN = '" + username + "' found.");
        }
        if (log.isDebugEnabled()) {
            log.debug("Successfully authenticated user: " + user);
        }
        return new org.springframework.security.core.userdetails.User(username, "", Collections.emptyList());
    }
}
