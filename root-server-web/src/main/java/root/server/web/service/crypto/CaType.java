package root.server.web.service.crypto;

/**
 * Certified authority purposes enumeration.
 * @author Alexey Chalov
 */
public enum CaType {

    CLIENT, USER
}
