package root.server.web.service.crypto;

/**
 * Interface for generating client certificates and private keys in PKCS12 format.
 * @author Alexey Chalov
 */
public interface CertificateGenerator {

    /**
     * Generates keystore with private key and client certificate in PKCS 12 format.
     * @param certCn CN RDN
     * @param caType certified authority type
     * @return generated keystore byte array
     */
    byte[] createPkcs12Store(String certCn, CaType caType);
}
