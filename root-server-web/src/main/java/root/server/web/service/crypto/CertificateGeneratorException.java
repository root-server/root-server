package root.server.web.service.crypto;

/**
 * General certificate generator exception.
 * @author Alexey Chalov
 */
@SuppressWarnings("serial")
public class CertificateGeneratorException extends RuntimeException {

    /**
     * Constructor.
     * @param message exception message
     * @param cause underlying cause
     */
    CertificateGeneratorException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor.
     * @param message exception message
     */
    public CertificateGeneratorException(String message) {
        super(message);
    }

    /**
     * Constructor.
     * @param cause underlying cause
     */
    CertificateGeneratorException(Throwable cause) {
        super(cause);
    }
}
