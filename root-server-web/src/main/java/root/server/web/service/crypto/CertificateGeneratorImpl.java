package root.server.web.service.crypto;

import static java.text.MessageFormat.format;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.security.auth.x500.X500Principal;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.RFC4519Style;
import org.bouncycastle.asn1.x509.ExtendedKeyUsage;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509ExtensionUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder;
import org.bouncycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

import lombok.Getter;

/**
 * {@link CertificateGenerator} service implementation.
 * @author Alexey Chalov
 */
@Service
public class CertificateGeneratorImpl implements CertificateGenerator {

    @Value("${service.crypto.gen-key-size}")
    private int keySize;
    @Value("${service.crypto.gen-validity}")
    private int validityDays;

    /* client certificate generator settings */
    @Value("${service.crypto.client.gen-password}")
    private String clientPassword;
    @Value("${service.crypto.client.gen-dn-template}")
    private String clientDnTemplate;
    @Value("${service.crypto.client.ca-key-store}")
    private String clientCaKeyStore;
    @Value("${service.crypto.client.ca-key-alias}")
    private String clientCaKeyAlias;
    @Value("${service.crypto.client.ca-key-password}")
    private String clientCaKeyPassword;
    @Value("${service.crypto.client.ca-store-password}")
    private String clientCaStorePassword;

    /* user certificate generator settings */
    @Value("${service.crypto.user.gen-password}")
    private String userPassword;
    @Value("${service.crypto.user.gen-dn-template}")
    private String userDnTemplate;
    @Value("${service.crypto.user.ca-key-store}")
    private String userCaKeyStore;
    @Value("${service.crypto.user.ca-key-alias}")
    private String userCaKeyAlias;
    @Value("${service.crypto.user.ca-key-password}")
    private String userCaKeyPassword;
    @Value("${service.crypto.user.ca-store-password}")
    private String userCaStorePassword;

    private Map<CaType, CaDataHolder> caTypeData;

    private KeyPairGenerator keyPairGenerator;
    private JcaContentSignerBuilder contentSignerBuilder;
    private JcaX509ExtensionUtils extensionUtils;
    private JcaX509CertificateConverter certificateConverter;

    private static final String BC_PROVIDER_NAME = "BC";
    private static final String SIGNATURE_ALGORITHM = "SHA256withRSA";
    private static final String KEY_PAIR_ALGORITHM = "RSA";
    private static final String KEY_STORE_TYPE = "PKCS12";

    @Override
    public byte[] createPkcs12Store(String certCn, CaType caType) {
        if (certCn == null || certCn.isBlank()) {
            throw new CertificateGeneratorException("No CN RDN provided.");
        }
        if (caType == null) {
            throw new CertificateGeneratorException("No CA type provided.");
        }
        CaDataHolder caData = caTypeData.get(caType);
        if (caData == null) {
            throw new CertificateGeneratorException("CA type '" + caType + "' is not configured.");
        }

        /* generate key pair */
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        PrivateKey privateKey = keyPair.getPrivate();
        PublicKey publicKey = keyPair.getPublic();

        /* generate CSR */
        PKCS10CertificationRequest csr = generateCsr(certCn, privateKey, publicKey);

        /* sign CSR on CA side */
        X509Certificate certificate = signCsr(csr, caData);

        /* generate keystore */
        return generateKeyStore(certCn, privateKey, certificate, caData);
    }

    /**
     * Generates CSR.
     * @param certCn certificate CN RDN
     * @param privateKey client private key
     * @param publicKey client public key
     * @return {@link PKCS10CertificationRequest} instance
     */
    private PKCS10CertificationRequest generateCsr(String certCn, PrivateKey privateKey, PublicKey publicKey) {
        try {
            PKCS10CertificationRequestBuilder pkcs10Builder = new JcaPKCS10CertificationRequestBuilder(
                new X500Principal(format(clientDnTemplate, certCn)), publicKey
            );
            ContentSigner contentSigner = contentSignerBuilder.build(privateKey);
            return pkcs10Builder.build(contentSigner);
        } catch (Exception e) {
            throw new CertificateGeneratorException(e);
        }
    }

    /**
     * Signs CRS.
     * <p>
     * <b>NOTE</b>, this code will not work correctly:
     * <pre>new X500Name(caCertificate.getSubjectDN().getName())</pre>
     * RDNs will appear in reverse order. Must use one of the following constructs:
     * <ul>
     * <li><pre>new X500Name(RFC4519Style.INSTANCE, caCertificate.getSubjectX500Principal().getName())</pre></li>
     * <li><pre>new JcaX509CertificateHolder((X509Certificate) caCertificate).getSubject()</pre></li>
     * </ul>
     * </p>
     * @param csr {@link PKCS10CertificationRequest} instance
     * @param caData {@link CaDataHolder} instance
     * @return signed X509 certificate
     */
    private X509Certificate signCsr(PKCS10CertificationRequest csr, CaDataHolder caData) {
        try {
            X509v3CertificateBuilder certGen = new X509v3CertificateBuilder(
                new X500Name(RFC4519Style.INSTANCE, caData.getCertificate().getSubjectX500Principal().getName()),
                BigInteger.valueOf(System.currentTimeMillis()),
                Date.from(Instant.now()),
                Date.from(Instant.now().plus(validityDays, ChronoUnit.DAYS)),
                csr.getSubject(),
                csr.getSubjectPublicKeyInfo()
            );
            certGen.addExtension(
                Extension.authorityKeyIdentifier, false,
                extensionUtils.createAuthorityKeyIdentifier(caData.getCertificate())
            );
            certGen.addExtension(
                Extension.extendedKeyUsage, false, new ExtendedKeyUsage(KeyPurposeId.id_kp_clientAuth)
            );
            X509CertificateHolder holder = certGen.build(contentSignerBuilder.build(caData.getPrivateKey()));

            return certificateConverter.getCertificate(holder);
        } catch (Exception e) {
            throw new CertificateGeneratorException(e);
        }
    }

    /**
     * Generates keystore from passed private key and signed certificate.
     * @param alias key alias
     * @param privateKey client private key
     * @param certificate client certificate
     * @param caData {@link CaDataHolder} instance
     * @return keystore content byte array
     */
    private byte[] generateKeyStore(String alias, PrivateKey privateKey,
                                    X509Certificate certificate, CaDataHolder caData) {
        try {
            KeyStore keyStore = KeyStore.getInstance(KEY_STORE_TYPE);
            keyStore.load(null);
            keyStore.setKeyEntry(
                alias, privateKey, clientPassword.toCharArray(),
                new X509Certificate[] {certificate, caData.getCertificate()}
            );
            keyStore.setCertificateEntry(clientCaKeyAlias, caData.getCertificate());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            keyStore.store(baos, clientPassword.toCharArray());
            return baos.toByteArray();
        } catch (Exception e) {
            throw new CertificateGeneratorException(e);
        }
    }

    /**
     * Initializes service.
     */
    @PostConstruct
    void initialize() {
        Security.addProvider(new BouncyCastleProvider());
        contentSignerBuilder = new JcaContentSignerBuilder(SIGNATURE_ALGORITHM).setProvider(BC_PROVIDER_NAME);
        certificateConverter = new JcaX509CertificateConverter().setProvider(BC_PROVIDER_NAME);
        try {
            extensionUtils = new JcaX509ExtensionUtils();
        } catch (NoSuchAlgorithmException e) {
            throw new CertificateGeneratorException("Error initializing extension utils.", e);
        }
        try {
            keyPairGenerator = KeyPairGenerator.getInstance(KEY_PAIR_ALGORITHM);
            keyPairGenerator.initialize(keySize);
        } catch (NoSuchAlgorithmException e) {
            throw new CertificateGeneratorException("Error initializing key pair generator.", e);
        }

        caTypeData = new HashMap<CaType, CaDataHolder>();
        KeyStore keyStore;
        try (FileInputStream caKeyStoreStream = new FileInputStream(clientCaKeyStore)) {
            keyStore = KeyStore.getInstance(KEY_STORE_TYPE);
            keyStore.load(caKeyStoreStream, clientCaStorePassword.toCharArray());
            caTypeData.put(
                CaType.CLIENT,
                new CaDataHolder(
                    (PrivateKey) keyStore.getKey(clientCaKeyAlias, clientCaKeyPassword.toCharArray()),
                    (X509Certificate) keyStore.getCertificate(clientCaKeyAlias)
                )
            );
        } catch (Exception e) {
            throw new CertificateGeneratorException("Error reading client CA keystore.", e);
        }

        try (FileInputStream caKeyStoreStream = new FileInputStream(userCaKeyStore)) {
            keyStore = KeyStore.getInstance(KEY_STORE_TYPE);
            keyStore.load(caKeyStoreStream, userCaStorePassword.toCharArray());
            caTypeData.put(
                CaType.USER,
                new CaDataHolder(
                    (PrivateKey) keyStore.getKey(userCaKeyAlias, userCaKeyPassword.toCharArray()),
                    (X509Certificate) keyStore.getCertificate(userCaKeyAlias)
                )
            );
        } catch (Exception e) {
            throw new CertificateGeneratorException("Error reading user CA keystore.", e);
        }
    }

    /**
     * CA data holder.
     * @author Alexey Chalov
     */
    @Getter
    @AllArgsConstructor
    private class CaDataHolder {
        private final PrivateKey privateKey;
        private final X509Certificate certificate;
    }
}
