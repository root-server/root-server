package root.server.web.service.validation;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import javax.annotation.PostConstruct;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import root.server.common.entity.Client;
import root.server.common.entity.FinancialOperationType;
import root.server.common.entity.base.IdEntity;
import root.server.common.validation.CorrectId;
import root.server.dao.ClassifierDao;
import root.server.dao.ClientDao;

/**
 * Not null and existing {@link IdEntity} identifier validator.
 * @author Alexey Chalov
 */
public class CorrectIdValidator implements ConstraintValidator<CorrectId, IdEntity> {

    @Autowired
    private ClientDao clientDao;
    @Autowired
    private ClassifierDao classifierDao;

    private Map<Class<? extends IdEntity>, Function<Long, Boolean>> idValidationFunctions;

    @Override
    public boolean isValid(IdEntity value, ConstraintValidatorContext context) {
        if (value == null || value.getId() == null) {
            return false;
        }
        Function<Long, Boolean> fn = idValidationFunctions.get(value.getClass());
        if (fn == null) {
            throw new IllegalArgumentException("Unexpected entity class: " + value.getClass() + ".");
        }
        return fn.apply(value.getId());
    }

    /**
     * Initializes map of object class to validation function.
     */
    @PostConstruct
    void initialize() {
        idValidationFunctions = new HashMap<>();
        idValidationFunctions.put(Client.class, clientDao::isClientWithIdExists);
        idValidationFunctions.put(FinancialOperationType.class, classifierDao::isVisibleFinancialOperationTypeExists);
    }
}
