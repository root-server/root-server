package root.server.web.util;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Root server utility class.
 * @author Alexey Chalov
 */
public final class RootServerUtil {

    /**
     * Disable default constructor for utility class.
     */
    private RootServerUtil() {
    }

    /**
     * Returns current user login.
     * @return current user login
     */
    public static String getCurrentUserLogin() {
        UserDetails userDetails =
            (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userDetails.getUsername();
    }
}
