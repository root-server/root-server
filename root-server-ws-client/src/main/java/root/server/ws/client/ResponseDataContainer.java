package root.server.ws.client;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Represents response data.
 * @author Alexey Chalov
 * @param <T> response data type
 */
@Getter
@RequiredArgsConstructor
public class ResponseDataContainer<T> {

    private final int httpStatus;
    private final T data;
}
