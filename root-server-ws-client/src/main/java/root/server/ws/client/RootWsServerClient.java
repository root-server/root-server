package root.server.ws.client;

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.http.HttpMethods;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import root.server.common.entity.Client;

/**
 * Root WS server client.
 * @author Alexey Chalov
 */
@Service
public class RootWsServerClient {

    @Value("${root.server.host}")
    private String rootServerHost;

    @Value("${root.server.path.client}")
    private String currentClientPath;

    @EndpointInject("direct:callRootServer")
    private ProducerTemplate producerTemplate;

    /**
     * Calls remote method to retrieve current client information.
     * @return server call result
     */
    @SuppressWarnings("unchecked")
    public ResponseDataContainer<Client> getCurrentClient() {
        Object response = producerTemplate.requestBodyAndHeaders(
            null, prepareHeaders(HttpMethods.GET, currentClientPath, Client.class.getSimpleName().toLowerCase())
        );
        if (response instanceof ResponseDataContainer) {
            /* error response */
            return (ResponseDataContainer<Client>) response;
        }
        return new ResponseDataContainer<Client>(HttpStatus.SC_OK, (Client) response);
    }

    /**
     * Creates error data container in case of root server HTTP response status code not equal to 200.
     * @param exchange {@link Exchange} instance
     */
    public void createErrorDataContainer(Exchange exchange) {
        int httpStatus = exchange.getIn().getHeader(Exchange.HTTP_RESPONSE_CODE, Integer.class);
        exchange.getMessage().setBody(new ResponseDataContainer<Object>(httpStatus, null));
    }

    /**
     * Prepares camel exchange headers for root server service call.
     * @param method {@link HttpMethods} constant
     * @param servicePath service path
     * @param jsonDataFormatId JSON data format, defined in camel context to unmarshal result
     * @return map of camel exchange headers
     */
    private Map<String, Object> prepareHeaders(HttpMethods method, String servicePath, String jsonDataFormatId) {
        Map<String, Object> headers = new HashMap<>();
        headers.put(Exchange.HTTP_METHOD, method.name());
        headers.put(Exchange.HTTP_URI, "https://" + rootServerHost + servicePath);
        headers.put("jsonDataFormat", jsonDataFormatId);
        return headers;
    }
}
