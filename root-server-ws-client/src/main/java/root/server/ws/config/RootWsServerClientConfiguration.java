package root.server.ws.config;

import org.apache.camel.component.http.HttpComponent;
import org.apache.camel.support.jsse.KeyManagersParameters;
import org.apache.camel.support.jsse.KeyStoreParameters;
import org.apache.camel.support.jsse.SSLContextParameters;
import org.apache.camel.support.jsse.TrustManagersParameters;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Root server client configuration.
 * @author Alexey Chalov
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = "root.server.ws.client")
@ImportResource("classpath:root-ws-server-client.xml")
public class RootWsServerClientConfiguration {

    @Value("${camel.ssl.keymanager.password}")
    private String keyManagerPassword;
    @Value("${camel.ssl.keymanager.keystore.resource}")
    private String keyManagerKeyStoreResource;
    @Value("${camel.ssl.keymanager.keystore.password}")
    private String keyManagerStorePassword;
    @Value("${camel.ssl.trustmanager.keystore.password}")
    private String trustManagerStorePassword;
    @Value("${camel.ssl.trustmanager.keystore.resource}")
    private String trustManagerKeyStoreResource;

    /**
     * Configures HTTP component to support X509 authentication on root server.
     * @return {@link HttpComponent} instance
     */
    @Bean("http4")
    HttpComponent httpComponent() {
        HttpComponent httpComponent = new HttpComponent();

        SSLContextParameters contextParameters = new SSLContextParameters();

        KeyManagersParameters keyManagers = new KeyManagersParameters();
        keyManagers.setKeyPassword(keyManagerPassword);
        KeyStoreParameters keyStore = new KeyStoreParameters();
        keyStore.setPassword(keyManagerStorePassword);
        keyStore.setResource(keyManagerKeyStoreResource);
        keyManagers.setKeyStore(keyStore);
        contextParameters.setKeyManagers(keyManagers);

        TrustManagersParameters trustManagers = new TrustManagersParameters();
        KeyStoreParameters trustStore = new KeyStoreParameters();
        trustStore.setPassword(trustManagerStorePassword);
        trustStore.setResource(trustManagerKeyStoreResource);
        trustManagers.setKeyStore(trustStore);
        contextParameters.setTrustManagers(trustManagers);

        httpComponent.setSslContextParameters(contextParameters);
        return httpComponent;
    }
}
