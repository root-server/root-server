package root.server.ws;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.http.HttpStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import lombok.extern.log4j.Log4j2;
import root.server.common.entity.Client;
import root.server.ws.client.ResponseDataContainer;
import root.server.ws.client.RootWsServerClient;
import root.server.ws.config.RootWsServerClientConfiguration;

/**
 * Root server WS client test collection.
 * @author Alexey Chalov
 */
@Log4j2
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RootWsServerClientConfiguration.class)
public class RootWsServerClientTest {

    @Autowired
    private RootWsServerClient wsClient;

    /**
     * Test to call {@link RootWsServerClient#getCurrentClient()}.
     */
    @Test
    public void testGetCurrentClient() {
        ResponseDataContainer<Client> clientContainer = wsClient.getCurrentClient();
        assertEquals(HttpStatus.SC_OK, clientContainer.getHttpStatus());
        assertNotNull(clientContainer.getData());
        log.info("Call to '/client' succeded. Result:");
        log.info(clientContainer.getData().toString());
    }
}
