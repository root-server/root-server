package root.server.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import root.server.dao.config.RootServerDaoConfiguration;
import root.server.ws.security.RootWsServerUserDetailsService;

/**
 * Spring boot application for root WS server.
 * @author Alexey Chalov
 */
@EnableWebSecurity
@SpringBootApplication
@EnableTransactionManagement
@Import(RootServerDaoConfiguration.class)
public class RootWsServer extends WebSecurityConfigurerAdapter {

    @Autowired
    private RootWsServerUserDetailsService userDetailsService;

    /**
     * Starts root WS server.
     * @param args program arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(RootWsServer.class, args);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().anyRequest()
            .authenticated()
            .and().csrf().disable()
            .x509().userDetailsService(userDetailsService);
    }
}
