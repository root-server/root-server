package root.server.ws.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Serves all unresolved paths.
 * @author Alexey Chalov
 */
@Controller
public class Error404Controller {

    /**
     * Returns 404 error code.
     * @return 404 error code
     */
    @RequestMapping(value = "/{[path:[^\\.]*}")
    public ResponseEntity<?> handle() {
        return ResponseEntity.notFound().build();
    }
}
