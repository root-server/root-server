package root.server.ws.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import root.server.common.entity.Client;
import root.server.dao.ClientDao;

/**
 * Client service methods collection.
 * @author Alexey Chalov
 */
@RestController
@RequestMapping(value = "/client")
public class ClientService {

    @Autowired
    private ClientDao clientDao;

    /**
     * Returns currently logged client.
     * @return {@link Client} instance
     */
    @GetMapping
    public Client getCurrentClient() {
        UserDetails userDetails =
            (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return clientDao.getByCN(userDetails.getUsername());
    }
}
