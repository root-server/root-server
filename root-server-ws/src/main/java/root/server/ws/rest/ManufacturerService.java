package root.server.ws.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Manufacturer service methods collection.
 * @author Alexey Chalov
 */
@RestController
@RequestMapping(value = "/manufacturer")
public class ManufacturerService {

}
