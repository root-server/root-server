package root.server.ws.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Part number service methods collection.
 * @author Alexey Chalov
 */
@RestController
@RequestMapping("/partnumber")
public class PartNumberService {

}
