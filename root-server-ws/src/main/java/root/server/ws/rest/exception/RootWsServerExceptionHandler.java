package root.server.ws.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import lombok.extern.log4j.Log4j2;

/**
 * General exception handler.
 * @author Alexey Chalov
 */
@Log4j2
@RestControllerAdvice
public class RootWsServerExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Internal server error exception handler.
     * @return {@link ResponseEntity} instance
     * @param t {@link Throwable} instance
     */
    @ExceptionHandler(Throwable.class)
    public ResponseEntity<?> internalServerError(Throwable t) {
        log.warn(t.getMessage(), t);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();

    }

    /**
     * Access denied exception handler.
     * @return {@link ResponseEntity} instance
     */
    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<?> accessDenied() {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }
}
