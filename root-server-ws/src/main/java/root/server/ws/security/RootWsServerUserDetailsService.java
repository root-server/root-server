package root.server.ws.security;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import lombok.extern.log4j.Log4j2;
import root.server.common.entity.Client;
import root.server.dao.ClientDao;

/**
 * {@link UserDetailsService} implementation.
 * @author Alexey Chalov
 */
@Log4j2
@Service
public class RootWsServerUserDetailsService implements UserDetailsService {

    @Autowired
    private ClientDao clientDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Client client = clientDao.getByCN(username);
        if (client == null) {
            throw new UsernameNotFoundException("No client with CN = '" + username + "' found.");
        }
        if (log.isDebugEnabled()) {
            log.debug("Successfully authenticated client: " + client);
        }
        return new org.springframework.security.core.userdetails.User(username, "", Collections.emptyList());
    }
}
